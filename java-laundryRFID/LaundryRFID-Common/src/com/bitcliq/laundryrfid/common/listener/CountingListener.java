/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.listener;

import com.bitcliq.laundryrfid.common.models.ContagemClassificacao;
import java.util.HashMap;

/**
 *
 * @author andre
 */
public interface CountingListener {
    void counterChanged(HashMap<Integer, ContagemClassificacao> contagem);
    void totalCounterChanged(int totalCount);
    void timerChanged(int timer);
    
}
