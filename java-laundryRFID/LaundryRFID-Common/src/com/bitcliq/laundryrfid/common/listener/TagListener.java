/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.listener;

import com.bitcliq.laundryrfid.common.models.Tag;

/**
 *
 * @author andre
 */
public interface TagListener {
    void foundTag(Tag tagFound);
}
