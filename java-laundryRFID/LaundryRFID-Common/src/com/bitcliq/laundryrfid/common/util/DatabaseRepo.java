/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. "++"
 */
package com.bitcliq.laundryrfid.common.util;

import com.bitcliq.laundryrfid.common.models.AssociacaoTagTipoRoupa;
import com.bitcliq.laundryrfid.common.models.Ciclo;
import com.bitcliq.laundryrfid.common.models.ContagemClassificacao;
import com.bitcliq.laundryrfid.common.models.Leitura;
import com.bitcliq.laundryrfid.common.models.Lote;
import com.bitcliq.laundryrfid.common.models.Tag;
import com.bitcliq.laundryrfid.common.models.TipoRoupa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andre
 */
public class DatabaseRepo {
    
    private Connection con = DBSingleton.getInstance().getConnection();
    
    //<editor-fold defaultstate="collapsed" desc=" TAGS ">
    
    // insere uma tag na BD
    public Tag insertTag(Tag tag){
        
        try {
            String insert = "INSERT INTO "+Tag.TABLE+" ("+Tag.F_TAG_ID+", "+Tag.F_LOTE_ID+") VALUES (?,?)";
            
            PreparedStatement insertStatement = con.prepareStatement(insert);
            insertStatement.setString(1, tag.getTagID());
            insertStatement.setInt(2, tag.getLoteID());
            insertStatement.executeUpdate();
            
            return tag;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException insertTag", ex);
        }
        return null;
    }
    
    // insere várias tags na BD relacionadas a um Lote
    public boolean insertTags(HashMap<String, Tag> tags, Lote lote){
        
        try {
            String insert = "INSERT INTO "+Tag.TABLE+" ("+Tag.F_TAG_ID+", "+Tag.F_LOTE_ID+") VALUES (?,?)";
            con.setAutoCommit(false);
            PreparedStatement statement = con.prepareStatement(insert);
            int i = 0;
            for(Map.Entry<String, Tag> tag : tags.entrySet()){
                statement.setString(1, tag.getValue().getTagID());
                statement.setInt(2, lote.getLoteID());
                statement.addBatch();
                i++;
                if (i % 100 == 0 || i == tags.size()) {
                    statement.executeBatch(); 
                    con.commit();
                }
            }
            con.setAutoCommit(true);
            return true;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException insertTags", ex);
            return false;
        }
    }
    
    // obtem a tag utilizando o ID da mesma, caso não seja encontrada devolve o mesmo objeto
    public Tag getTag(Tag tag){
        try {
            String select = "SELECT TOP 1 "+Tag.TF_TAG_ID+", "+Lote.TF_LOTE_ID+", "+AssociacaoTagTipoRoupa.TF_TIPO_ROUPA_ID+", "+AssociacaoTagTipoRoupa.TF_DATA+", "+TipoRoupa.TF_NOME+", " + TipoRoupa.F_REF_PRIMAVERA + ", "+Lote.TF_FABRICANTE+", "+Lote.TF_REF_ENCOMENDA+" "
                    + "FROM "+Tag.TABLE+" JOIN "+AssociacaoTagTipoRoupa.TABLE+" ON "+AssociacaoTagTipoRoupa.TF_TAG_ID+" = "+Tag.TF_TAG_ID+" "
                    + "JOIN "+TipoRoupa.TABLE+" ON "+TipoRoupa.TF_TIPO_ROUPA_ID+" = "+AssociacaoTagTipoRoupa.TF_TIPO_ROUPA_ID+" "
                    + "JOIN "+Lote.TABLE+" ON "+Lote.TF_LOTE_ID+" = "+Tag.TF_LOTE_ID+" "
                    + "WHERE "+Tag.TF_TAG_ID+" = ? ORDER BY "+AssociacaoTagTipoRoupa.TF_DATA+" DESC";
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setString(1, tag.getTagID());
            
            ResultSet rs = statement.executeQuery();
            
            while(rs.next()){
                
                tag.setLoteID(rs.getInt(Lote.F_LOTE_ID));
                
                TipoRoupa roupa = new TipoRoupa();
                roupa.setNome(rs.getString(TipoRoupa.F_NOME));
                roupa.setTipoRoupaID(rs.getInt(TipoRoupa.F_TIPO_ROUPA_ID));
                roupa.setRefPrimavera(rs.getString(TipoRoupa.F_REF_PRIMAVERA));
                tag.setUltimoTipoRoupa(roupa);
                
                Lote lote = new Lote();
                lote.setFabricante(rs.getString(Lote.F_FABRICANTE));
                lote.setRefEncomenda(rs.getString(Lote.F_REF_ENCOMENDA));
                lote.setLoteID(rs.getInt(Lote.F_LOTE_ID));
                tag.setLote(lote);
                
                return tag;
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException getTag", ex);
        }
        
        return tag;
    }
    
    // verifica se a tag está registada na base de dados
    public boolean isTagRegistered(String tag){
        try {
            String select = "SELECT "+Tag.F_TAG_ID+" FROM "+Tag.TABLE+" WHERE "+Tag.F_TAG_ID+" = ?";
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setString(1, tag);
            
            ResultSet rs = statement.executeQuery();
            
            while(rs.next()){
                return true;
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException isTagRegistered", ex);
        }
        return false;
    }
    
    // obtem as tags que têm um fornecedor registado (com limite)
    public ArrayList<Tag> getTagsComFornecedor(int limite){
        ArrayList<Tag> tags = new ArrayList<>();
        try {
            String select = "SELECT TOP (?) "+Tag.TF_TAG_ID+", "+Tag.TF_LOTE_ID+", "+Lote.TF_FABRICANTE+", "+Lote.TF_REF_ENCOMENDA+" FROM "+Tag.TABLE+" "
                    + "LEFT JOIN "+AssociacaoTagTipoRoupa.TABLE+" ON "+Tag.TF_TAG_ID+" = "+AssociacaoTagTipoRoupa.TF_TAG_ID+" "
                    + "JOIN "+Lote.TABLE+" ON "+Tag.TF_LOTE_ID+" = "+Lote.TF_LOTE_ID+" "
                    + "WHERE "+AssociacaoTagTipoRoupa.TF_TAG_ID+" IS NULL";
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setInt(1, limite);
            
            ResultSet rs = statement.executeQuery();
            Tag tag;
            while(rs.next()){
                tag = new Tag();
                
                tag.setTagID(rs.getString(Tag.F_TAG_ID));
                tag.setLoteID(rs.getInt(Tag.F_LOTE_ID));
                
                Lote lote = new Lote();
                lote.setFabricante(rs.getString(Lote.F_FABRICANTE));
                lote.setRefEncomenda(rs.getString(Lote.F_REF_ENCOMENDA));
                lote.setLoteID(rs.getInt(Tag.F_LOTE_ID));
                tag.setLote(lote);
                
                tags.add(tag);
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException getTagsComFornecedor", ex);
        }
        
        return tags;
    }
    
    public ArrayList<Tag> getTagsComFornecedor(){
        return getTagsComFornecedor(20);
    }
    
    // obtem as tags com o tipo de roupa registado
    public ArrayList<Tag> getTagsComTipoRoupa(int limite){
        ArrayList<Tag> tags = new ArrayList<>();
        try {
            String select = "SELECT TOP (?) "+Tag.TF_TAG_ID+", MAX("+Lote.TF_LOTE_ID+") AS "+Lote.F_LOTE_ID+", MAX("+Lote.F_FABRICANTE+") AS "+Lote.F_FABRICANTE+", MAX("+Lote.F_REF_ENCOMENDA+") AS "+Lote.F_REF_ENCOMENDA+" FROM "+Tag.TABLE+" "
                    + "JOIN "+AssociacaoTagTipoRoupa.TABLE+" ON "+Tag.TF_TAG_ID+" = "+AssociacaoTagTipoRoupa.TF_TAG_ID+" "
                    + "JOIN "+Lote.TABLE+" ON "+Tag.TF_LOTE_ID+" = "+Lote.TF_LOTE_ID+" "
                    + "JOIN "+TipoRoupa.TABLE+" ON "+TipoRoupa.TF_TIPO_ROUPA_ID+" = "+AssociacaoTagTipoRoupa.TF_TIPO_ROUPA_ID+" "
                    + "GROUP BY "+Tag.TF_TAG_ID;
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setInt(1, limite);
            
            ResultSet rs = statement.executeQuery();
            Tag tag;
            
            while(rs.next()){
                tag = new Tag();
                
                tag.setTagID(rs.getString(Tag.F_TAG_ID));
                tag.setLoteID(rs.getInt(Lote.F_LOTE_ID));
                
                Lote lote = new Lote();
                lote.setFabricante(rs.getString(Lote.F_FABRICANTE));
                lote.setRefEncomenda(rs.getString(Lote.F_REF_ENCOMENDA));
                lote.setLoteID(rs.getInt(Lote.F_LOTE_ID));
                tag.setLote(lote);
                
                tags.add(tag);
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException getTagsComTipoRoupa", ex);
        }
        
        return tags;
    }
    
    public ArrayList<Tag> getTagsComTipoRoupa(){
        return getTagsComTipoRoupa(20);
    }
    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" LOTES ">
    
    // insere um Lote na BD
    public Lote insertLote(Lote lote){
        
        try {
            String insert = "INSERT INTO "+Lote.TABLE+" ("+Lote.F_FABRICANTE+", "+Lote.F_REF_ENCOMENDA+") VALUES (?,?)";
            
            PreparedStatement insertStatement = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, lote.getFabricante());
            insertStatement.setString(2, lote.getRefEncomenda());
            insertStatement.executeUpdate();
            
            try(ResultSet generatedKeys = insertStatement.getGeneratedKeys()){
                if(generatedKeys.next()){
                    lote.setLoteID(generatedKeys.getInt(1));
                }
            }
            
            return lote;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException insertLote", ex);
        }
        return null;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" TIPOS DE ROUPA ">
    
    // obtem os tipos de roupa existentes
    public ArrayList<TipoRoupa> getTiposDeRoupa(){
        
        ArrayList<TipoRoupa> roupas = new ArrayList<>();
        
        try {
            String select = "SELECT "+TipoRoupa.F_TIPO_ROUPA_ID+", "+TipoRoupa.F_NOME+", " + TipoRoupa.F_REF_PRIMAVERA + " FROM "+TipoRoupa.TABLE;
            
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery(select);
            
            TipoRoupa roupa;
            
            while(rs.next()){
                
                roupa = new TipoRoupa();
                roupa.setNome(rs.getString(TipoRoupa.F_NOME));
                roupa.setTipoRoupaID(rs.getInt(TipoRoupa.F_TIPO_ROUPA_ID));
                roupa.setRefPrimavera(rs.getString(TipoRoupa.F_REF_PRIMAVERA));
                
                roupas.add(roupa);
            }
            
            return roupas;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException getTiposDeRoupa", ex);
        }
        return null;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" ASSOCIAÇÃO TAG A TIPO DE ROUPA ">
    
    // insere a associação entre muitas tags e o novo tipo de roupa
    public boolean insertAssociacaoTagRoupa(HashMap<String, Tag> tags, TipoRoupa roupa){
        
        try {
            String insert = "INSERT INTO "+AssociacaoTagTipoRoupa.TABLE+" ("+AssociacaoTagTipoRoupa.F_DATA+", "+AssociacaoTagTipoRoupa.F_TAG_ID+", "+AssociacaoTagTipoRoupa.F_TIPO_ROUPA_ID+") VALUES (?, ?, ?)";
            con.setAutoCommit(false);
            PreparedStatement statement = con.prepareStatement(insert);
            int i = 0;
            for(Map.Entry<String, Tag> tag : tags.entrySet()){
                
                DateFormat df = GeneralUtils.formatDatabaseDateTime();
                
                statement.setString(1, GeneralUtils.getDateNow());
                statement.setString(2, tag.getValue().getTagID());
                statement.setInt(3, roupa.getTipoRoupaID());
                statement.addBatch();
                i++;
                if (i % 100 == 0 || i == tags.size()) {
                    statement.executeBatch(); 
                    con.commit();
                }
            }
            con.setAutoCommit(true);
            return true;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException insertAssociacaoTagRoupa", ex);
            return false;
        }
    }
    
    public boolean isTipoRoupaRegisted(TipoRoupa roupa){
        try {
            String select = "SELECT "+TipoRoupa.F_TIPO_ROUPA_ID+" FROM "+TipoRoupa.TABLE+" WHERE "+TipoRoupa.F_TIPO_ROUPA_ID+" = ?";
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setInt(1, roupa.getTipoRoupaID());
            
            ResultSet rs = statement.executeQuery();
            
            while(rs.next()){
                return true;
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException isTipoRoupaRegisted", ex);
        }
        return false;
    }
    
    
    public ArrayList<AssociacaoTagTipoRoupa> getTiposRoupaTag(String tag){
        ArrayList<AssociacaoTagTipoRoupa> roupas = new ArrayList<>();
        
        try {
            String select = "SELECT "+AssociacaoTagTipoRoupa.TF_DATA+", "+AssociacaoTagTipoRoupa.TF_TAG_ID+", "+TipoRoupa.TF_TIPO_ROUPA_ID+", "+TipoRoupa.TF_NOME+", " + TipoRoupa.F_REF_PRIMAVERA + " "
                    + "FROM "+AssociacaoTagTipoRoupa.TABLE+" "
                    + "JOIN "+TipoRoupa.TABLE+" ON "+TipoRoupa.TF_TIPO_ROUPA_ID+" = "+AssociacaoTagTipoRoupa.TF_TIPO_ROUPA_ID+" "
                    + "WHERE "+AssociacaoTagTipoRoupa.TF_TAG_ID+" = ? "
                    + "ORDER BY "+AssociacaoTagTipoRoupa.TF_DATA+" DESC";
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setString(1, tag);
            
            ResultSet rs = statement.executeQuery();
            
            Date oldDate = null;
            AssociacaoTagTipoRoupa assocTipoRoupa;
            TipoRoupa tipoRoupa;
            
            while(rs.next()){
                try {
                    assocTipoRoupa = new AssociacaoTagTipoRoupa();
                    assocTipoRoupa.setTagID(rs.getString(AssociacaoTagTipoRoupa.F_TAG_ID));
                    tipoRoupa = new TipoRoupa(rs.getInt(TipoRoupa.F_TIPO_ROUPA_ID), rs.getString(TipoRoupa.F_NOME), rs.getString(TipoRoupa.F_REF_PRIMAVERA));
                    assocTipoRoupa.setTipoRoupaID(tipoRoupa.getTipoRoupaID());
                    assocTipoRoupa.setTipoRoupa(tipoRoupa);
                    assocTipoRoupa.setTimestamp(GeneralUtils.formatDatabaseDateTime().parse(rs.getString(AssociacaoTagTipoRoupa.F_DATA)));
                    if(oldDate != null) {
                        assocTipoRoupa.setTimestampEnd(oldDate);
                    }
                    oldDate = new Date();
                    oldDate = GeneralUtils.formatDatabaseDateTime().parse(rs.getString(AssociacaoTagTipoRoupa.F_DATA));
                    
                    roupas.add(assocTipoRoupa);
                    
                } catch (ParseException ex) {
                    GeneralUtils.getLogger().fatal("SQLException getTiposRoupaTag", ex);
                }
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException getTiposRoupaTag", ex);
        }
        
        
        return roupas;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" CONTAGEM CLASSIFICAÇÃO ">
    
    // insere a associação entre muitas tags e o novo tipo de roupa
    public boolean insertContagem(Ciclo ciclo, HashMap<Integer, ContagemClassificacao> contagem){
        
        try {
            String insert = "INSERT INTO "+ContagemClassificacao.TABLE+" ("+ContagemClassificacao.F_CICLO_ID+ ", " +ContagemClassificacao.F_CONTADOR+", "+ContagemClassificacao.F_TIPO_ROUPA_ID+", "+ContagemClassificacao.F_PROCESSADO+") VALUES (?, ?, ?, 0)";
            con.setAutoCommit(false);
            PreparedStatement statement = con.prepareStatement(insert);
            int i = 0;
            for(ContagemClassificacao classificacao : contagem.values()){
                statement.setInt(1, ciclo.getCicloID());
                statement.setInt(2, classificacao.getContador());
                statement.setInt(3, classificacao.getTipoRoupaID());
                statement.addBatch();
                i++;
                if (i % 100 == 0 || i == contagem.size()) {
                    statement.executeBatch(); 
                    con.commit();
                }
            }
            con.setAutoCommit(true);
            return true;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException insertContagem", ex);
            return false;
        }
    }
    
    public ArrayList<ContagemClassificacao> getContagensNaoProcessadas(int limite){
        ArrayList<ContagemClassificacao> contagens = new ArrayList<>();
        
        try {
            String select = "SELECT TOP (?) "+ContagemClassificacao.TF_CICLO_ID+", "+ContagemClassificacao.TF_CONTADOR+", "+ContagemClassificacao.TF_PROCESSADO+", "+TipoRoupa.TF_TIPO_ROUPA_ID+", "+TipoRoupa.F_NOME+", " + TipoRoupa.F_REF_PRIMAVERA + " "
                    + "FROM "+ContagemClassificacao.TABLE+" JOIN "+TipoRoupa.TABLE+" "
                    + "ON "+TipoRoupa.TF_TIPO_ROUPA_ID+" = "+ContagemClassificacao.TF_TIPO_ROUPA_ID+" "
                    + "WHERE "+ContagemClassificacao.TF_PROCESSADO+" = 0";
            
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setInt(1, limite > 0 ? limite : 20);
            
            ResultSet rs = statement.executeQuery();
            
            TipoRoupa tipoRoupa;
            ContagemClassificacao contagem;
            
            while(rs.next()){
                contagem = new ContagemClassificacao();
                tipoRoupa = new TipoRoupa();
                
                contagem.setCicloID(rs.getInt(ContagemClassificacao.F_CICLO_ID));
                contagem.setContador(rs.getInt(ContagemClassificacao.F_CONTADOR));
                contagem.setProcessadoInt(rs.getInt(ContagemClassificacao.F_PROCESSADO));
                contagem.setTipoRoupaID(rs.getInt(TipoRoupa.F_TIPO_ROUPA_ID));
                
                tipoRoupa.setTipoRoupaID(rs.getInt(TipoRoupa.F_TIPO_ROUPA_ID));
                tipoRoupa.setNome(rs.getString(TipoRoupa.F_NOME));
                tipoRoupa.setRefPrimavera(rs.getString(TipoRoupa.F_REF_PRIMAVERA));
                
                contagem.setTipoRoupa(tipoRoupa);
                
                contagens.add(contagem);
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException getContagensNaoProcessadas", ex);
        }
        
        return contagens;
    }
    
    public ArrayList<ContagemClassificacao> getContagensNaoProcessadas(){
        return getContagensNaoProcessadas(-1);
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc=" LEITURAS ">
    
    // insere a associação entre muitas tags e o novo tipo de roupa
    public Leitura insertLeitura(Leitura leitura){
        try {
            String insert = "INSERT INTO "+Leitura.TABLE+" ("+Leitura.F_DATA+", "+Leitura.F_TAG_ID+", "+Leitura.F_ENTRADA_OU_SAIDA+") VALUES (?, ?, ?)";
            PreparedStatement statement = con.prepareStatement(insert);
            statement.setTimestamp(1, GeneralUtils.getTimestampNow());
            statement.setString(2, leitura.getTagID());
            statement.setBoolean(3, leitura.isEntrada());
            statement.executeUpdate();
            
            return leitura;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException insertLeitura", ex);
            return null;
        }
    }
    
    public boolean tagEntradaOuSaida(Leitura leitura){
        
        try {
            String insert = "SELECT TOP 1 "+Leitura.F_DATA+", "+Leitura.F_TAG_ID+", "+Leitura.F_ENTRADA_OU_SAIDA+" FROM "+Leitura.TABLE+" WHERE "+Leitura.F_TAG_ID+" = ? ORDER BY "+Leitura.F_DATA+" DESC";
            PreparedStatement statement = con.prepareStatement(insert);
            statement.setString(1, leitura.getTagID());
            ResultSet rs = statement.executeQuery();
            
            while(rs.next()){
                return !rs.getBoolean(Leitura.F_ENTRADA_OU_SAIDA);
            }
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException tagEntradaOuSaida", ex);
        }
        return true;
    }
    
    
    public ArrayList<Leitura> getLeiturasTagDatas(String tag, Date start, Date end){
        ArrayList<Leitura> leituras = new ArrayList<>();
        
        Date startTimestamp = start;
        Date endTimestamp = null;
        if(end != null)
            endTimestamp = end;
        try {
            String select = "SELECT "+Leitura.F_DATA+", "+Leitura.F_ENTRADA_OU_SAIDA+" FROM "+Leitura.TABLE+" WHERE "+Leitura.F_TAG_ID+" = ? AND "+Leitura.F_DATA+" >= ?";
            if(end != null)
                select += " AND "+Leitura.F_DATA+" <= ?";
            
            
            
            PreparedStatement statement = con.prepareStatement(select);
            statement.setString(1, tag);
            statement.setDate(2, new java.sql.Date(start.getTime()));
            if(end != null)
                statement.setDate(3, new java.sql.Date(end.getTime()));
            
            ResultSet rs = statement.executeQuery();
            Leitura leitura;
            
            while(rs.next()){
                leitura = new Leitura();
                
                java.sql.Date m = rs.getDate(Leitura.F_DATA);
                
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                try {
                    leitura.setTimestamp(format.parse(rs.getString(Leitura.F_DATA)));
                } catch (ParseException ex) {
                    Logger.getLogger(DatabaseRepo.class.getName()).log(Level.SEVERE, null, ex);
                }

                leitura.setTagID(tag);
                leitura.setEntradaSaidaInt(rs.getInt(Leitura.F_ENTRADA_OU_SAIDA));

                leituras.add(leitura);
            }
            
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException getLeiturasTagDatas", ex);
        }
        
        
        return leituras;
    }
    
    //</editor-fold>
    
    public boolean clearDatabase(){
        
        try {
            
            Statement statement = con.createStatement();
            
            statement.executeUpdate("TRUNCATE TABLE " + AssociacaoTagTipoRoupa.TABLE);
            statement.executeUpdate("TRUNCATE TABLE " + ContagemClassificacao.TABLE);
            statement.executeUpdate("TRUNCATE TABLE " + Leitura.TABLE);
            statement.executeUpdate("DELETE FROM " + Tag.TABLE);
            
            return true;
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException clearDatabase", ex);
            return false;
        }
    }
    
    public Ciclo createCycle() {
        try {
            
            PreparedStatement statement = con.prepareStatement("INSERT INTO "+ Ciclo.TABLE + "("+Ciclo.F_END_DATE+") VALUES (GETDATE())", Statement.RETURN_GENERATED_KEYS);

            int affected = statement.executeUpdate();
            
            if(affected != 0) {
                ResultSet keys = statement.getGeneratedKeys();
                if(keys.next()){
                    Ciclo ciclo = new Ciclo(keys.getInt(1), new Date());
                    return ciclo;
                }
            }
                    
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException createCycle", ex);
        }
        return null;
    }
    
    public void cleanContagemClassificacao() {
        try {
            Statement statement = con.createStatement();
            statement.executeUpdate("TRUNCATE TABLE " + ContagemClassificacao.TABLE);
                    
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException createCycle", ex);
        }
    }
    
}
