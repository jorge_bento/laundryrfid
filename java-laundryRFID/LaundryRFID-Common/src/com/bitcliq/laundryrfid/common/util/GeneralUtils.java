/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author andre
 */
public class GeneralUtils {

    // numero aleatório entre min e max
    public static int randomInt(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min) + min;
    }

    // formata as horas
    public static SimpleDateFormat formatHours() {
        return new SimpleDateFormat("HH:mm:ss");
    }

    // formata para ano-mes-dia hora-minuto-segundo
    public static SimpleDateFormat formatDatabaseDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public static String formatDBDateTime(Date date) {
        return formatDatabaseDateTime().format(date);
    }

    public static long formatDBUnixTime(Date date) {
        return date.getTime() / 1000;
    }

    // data agora formatada no formato ano-mes-dia hora-minuto-segundo
    public static String getDateNow() {
        return formatDatabaseDateTime().format(Calendar.getInstance().getTime());
    }

    public static Date getNow() {
        return Calendar.getInstance().getTime();
    }
    
    public static long unixTimeNow(){
        //return getNow().getTime();
        return formatDBUnixTime(getNow());
    }
    
    public static String formatedDate(Date date){
        if(date == null) return "";
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date);
    }
    
    public static Timestamp getTimestamp(Date date){
        Timestamp timestamp = new Timestamp(date.getTime());
        return timestamp;
    }
    
    public static Timestamp getTimestampNow(){
        return getTimestamp(new Date());
    }

    // devolve o valor que vêm em "value" ou se este for nulo devolve o defaultValue
    public static String thisOrDefault(String value, String defaultValue) {
        return value != null ? value : defaultValue;
    }

    public static String md5(String s) {
        if(s == null) return "";
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(s.getBytes(), 0, s.length());
            BigInteger i = new BigInteger(1, m.digest());
            return String.format("%1$032x", i);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static String path(){
        String pathConfig = "";
        try {
            File fc;
            fc = new File(GeneralUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            pathConfig = fc.getParent();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }
        return pathConfig;
    }
    
    public static Logger getLogger(){
        Logger logger = null;
        String path = path() + "/log4j2.xml";
        File file = new File(path);
        if(!file.exists()){
            
            try (PrintWriter writer = new PrintWriter(path, "UTF-8")) {
                
                writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<Configuration status=\"WARN\">\n" +
                    "  <Appenders>\n" +
                    "    <Console name=\"Console\" target=\"SYSTEM_OUT\">\n" +
                    "      <PatternLayout pattern=\"%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n\"/>\n" +
                    "    </Console>\n" +
                    "  </Appenders>\n" +
                    "  <Loggers>\n" +
                    "    <Root level=\"debug\">\n" +
                    "      <AppenderRef ref=\"Console\"/>\n" +
                    "    </Root>\n" +
                    "  </Loggers>\n" +
                    "</Configuration>");
                
                writer.close();
                
            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                GeneralUtils.getLogger().fatal("Exception", ex);
            }
            
        }
        
        System.setProperty("log4j2.configurationFile", path);

        
        logger = LogManager.getLogger();
        
        return logger;
    }
    
   
}
