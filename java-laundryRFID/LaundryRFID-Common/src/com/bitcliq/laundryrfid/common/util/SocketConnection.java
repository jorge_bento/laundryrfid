/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.util;

import com.bitcliq.laundryrfid.common.models.Config;
import com.bitcliq.laundryrfid.common.models.ContagemClassificacao;
import com.bitcliq.laundryrfid.common.models.LeituraTagsDatas;
import com.bitcliq.laundryrfid.common.models.Lote;
import com.bitcliq.laundryrfid.common.models.SocketCode;
import com.bitcliq.laundryrfid.common.models.SocketMessage;
import com.bitcliq.laundryrfid.common.models.TipoRoupa;
import com.google.gson.Gson;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author andre
 */
public class SocketConnection {

    private Socket socket = null;
    private final Config config = Singleton.getInstance().getConfig();
    private int reconnectAttempts = 0;
    private final int maxReconnectAttempts = 5;
    private final Logger LOG = GeneralUtils.getLogger();

    public interface SocketMessageCallback {

        void onMessageRecieved(SocketMessage message);
    }

    public interface SocketConnectedCallback {

        void onConnected();
    }

    public interface SocketReplyCallback {

        void onReply(SocketMessage message);
    }

    public SocketConnection() {
        if (socket == null || socket != null && !socket.connected()) {
            try {
                IO.Options opts = new IO.Options();
                opts.reconnectionAttempts = maxReconnectAttempts;
                socket = IO.socket(config.getSocketLocation(), opts);
            } catch (URISyntaxException ex) {
                LOG.fatal("EXCEPTION CONNECT()", ex);
            }
        }
    }
    

    public void connect() {
        if (!connected()) {
            socket.connect();
        }
    }

    public void disconnect() {
        if (connected()) {
            socket.disconnect();
            socket.off();
        }
    }

    public boolean connected() {
        return socket != null && socket.connected();
    }

    public void on(String event, SocketMessageCallback callback) {
        if (connected()) {
            socket.on(event, new Emitter.Listener() {
                @Override
                public void call(Object... os) {
                    String msg = os[0].toString();
                    SocketMessage message = new SocketMessage();
                    try {
                        JSONObject obj = new JSONObject(msg);
                        message.setCode(obj.getInt("code"));
                        message.setNeedsReply(obj.has("replyTo"));
                        message.setMessage(obj);
                    } catch (JSONException ex) {
                        LOG.fatal("JSONException ON(). Message: ("+msg+") ", ex);
                    }
                    if (message.getCode() != -1) {
                        callback.onMessageRecieved(message);
                    }
                }
            });
        }

    }

    public void onConnect(SocketConnectedCallback callback) {
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... os) {
                callback.onConnected();
            }
        });
    }

    public void onceConnect(SocketConnectedCallback callback) {
        socket.once(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... os) {
                socket.off(Socket.EVENT_CONNECT, this);
                callback.onConnected();
            }
        });
    }
    
    public void onConnectionFailed(SocketConnectedCallback callback) {
        socket.on(Socket.EVENT_RECONNECTING, new Emitter.Listener() {
            @Override
            public void call(Object... os) {
                reconnectAttempts++;
                
                if(reconnectAttempts != maxReconnectAttempts){
                    LOG.error("Problema ao conectar com o servidor de Sockets. ("+reconnectAttempts+"/"+maxReconnectAttempts+")");
                } else {
                    socket.off(Socket.EVENT_RECONNECTING, this);
                    callback.onConnected();
                }
            }
        });
    }

    public void onReply(SocketReplyCallback callback) {
        socket.once("reply", new Emitter.Listener() {
            @Override
            public void call(Object... os) {
                String msg = os[0].toString();
                SocketMessage message = new SocketMessage();
                try {
                    JSONObject obj = new JSONObject(msg);
                    message.setCode(obj.getInt("code"));
                    message.setMessage(obj);
                } catch (JSONException ex) {
                    LOG.fatal("JSONException ON(). Message: ("+msg+") ", ex);
                }
                if (message.getCode() != -1) {
                    callback.onReply(message);
                }
            }
        });
    }

    public void reply(int code, SocketMessage message) {
        if (connected()) {
            try {

                JSONObject obj = message.getMessage();
                obj.put("code", code);

                socket.emit("reply", message.getMessage().toString());
            } catch (JSONException ex) {
                LOG.fatal("JSONException reply()", ex);
            }
        }
    }

    private void emit(JSONObject obj, SocketReplyCallback callback) {
        if (connected()) {
            final String replyID = GeneralUtils.md5(GeneralUtils.unixTimeNow() + "-" + obj.toString() + "-" + GeneralUtils.randomInt(0, 99999));
            try {
                if (callback != null) {
                    obj.put("replyTo", replyID);
                }
            } catch (JSONException ex) {
                LOG.fatal("JSONException emit()", ex);
            }
            if (callback != null) {
                final Response response = new Response();

                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (!response.hasResponse()) {
                            response.setResponse(true);
                            
                            callback.onReply(null);
                        }
                        timer.cancel();

                    }
                }, 10000);

                socket.on("reply", new Emitter.Listener() {
                    @Override
                    public void call(Object... os) {
                        
                        timer.cancel();
                        
                        if (!response.hasResponse()) {
                            response.setResponse(true);
                            
                            String msg = os[0].toString();
                            SocketMessage message = new SocketMessage();
                            try {
                                JSONObject obj = new JSONObject(msg);
                                message.setCode(obj.getInt("code"));
                                message.setMessage(obj);
                                if (message.getCode() != -1 && obj.getString("replyTo").equals(replyID)) {
                                    socket.off("reply", this);
                                    callback.onReply(message);
                                }
                            } catch (JSONException ex) {
                                LOG.fatal("JSONException ON(). Message: ("+msg+") ", ex);
                            }
                        }

                    }
                });
            }
            emit(obj.toString());

        }
    }

    private void emit(String channel, Object object) {
        if (connected()) {
            socket.emit(channel, object);
        }
    }

    private void emit(Object object) {
        if (connected()) {
            emit("message", object);
        }
    }

    public void emitCode(int code, SocketReplyCallback callback) {
        /*if (connected()) {JSONObject obj = new JSONObject();final String replyID = GeneralUtils.md5(GeneralUtils.unixTimeNow() + "-" + code + "-" + GeneralUtils.randomInt(0, 99999));try {obj.put("code", code);obj.put("replyTo", replyID);} catch (JSONException ex) {Logger.getLogger(SocketConnection.class.getName()).log(Level.SEVERE, null, ex);}emit(obj.toString());socket.on("reply", new Emitter.Listener() {@Overridepublic void call(Object... os) {String msg = os[0].toString();SocketMessage message = new SocketMessage();try {System.out.println(msg);JSONObject obj = new JSONObject(msg);message.setCode(obj.getInt("code"));message.setMessage(obj);if (message.getCode() != -1 && obj.getString("replyTo").equals(replyID)) {socket.off("reply", this);callback.onReply(message);}} catch (JSONException ex) {System.out.println(msg);Logger.getLogger(SocketConnection.class.getName()).log(Level.SEVERE, null, ex);}}});}*/
        if (connected()) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("code", code);
                emit(obj, callback);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitCode()", ex);
            }
        }
    }

    public void emitChangeMockupReadingType(int newType, SocketReplyCallback callback) {
        if (connected()) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("code", SocketCode.SET_MOCK_READING_TYPE);
                obj.put("type", newType);
                emit(obj, callback);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitChangeMockupReadingType()", ex);
            }
        }
    }

    public void emitChangeReadingType(boolean isMock, SocketReplyCallback callback) {
        if (connected()) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("code", SocketCode.SET_MOCK_READING);
                obj.put("isMock", isMock);
                emit(obj, callback);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitChangeReadingType()", ex);
            }
        }

    }

    public void emitCode(int code) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", code);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitCode()", ex);
            }
            emit(obj.toString());
        }

    }

    public void emitTotalCounterChanged(int comRoupa, int semRoupa) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.UPDATED_COUNTER);

                obj.put("comRoupa", comRoupa);
                obj.put("semRoupa", semRoupa);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitTotalCounterChanged()", ex);
            }
            emit(obj.toString());
        }
    }

    public void emitPiecesCounterChanged(HashMap<Integer, ContagemClassificacao> contagem) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.UPDATED_PIECES_COUNTER);
                obj.put("count", new Gson().toJson(contagem));
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitPiecesCounterChanged()", ex);
            }
            emit(obj.toString());
        }
    }

    public void emitTimerChanged(int timer, int limit) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.UPDATED_TIMER);

                obj.put("timer", timer);
                obj.put("limit", limit);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitTimerChanged()", ex);
            }
            emit(obj.toString());
        }
    }

    public void emitClearReadTags(SocketReplyCallback callback) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.CLEAR_READ_TAGS);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitClearReadTags()", ex);
            }
            emit(obj, callback);
        }
    }

    public void emitListTypeClothing(ArrayList<TipoRoupa> roupas) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.LIST_TYPES_CLOTHING);
                obj.put("list", new Gson().toJson(roupas));
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitListTypeClothing()", ex);
            }
            emit(obj.toString());
        }
    }

    public void emitRegisterProviderInformation(Lote lote, SocketReplyCallback callback) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.REGISTER_TAGS_PROVIDER);
                obj.put("lote", new Gson().toJson(lote));
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitRegisterProviderInformation()", ex);
            }
            emit(obj, callback);
        }
    }

    public void emitRegisterClothingInformation(TipoRoupa roupa, SocketReplyCallback callback) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.REGISTER_TAGS_CLOTHING);
                obj.put("tipoRoupa", new Gson().toJson(roupa));
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitRegisterClothingInformation()", ex);
            }
            emit(obj, callback);
        }
    }
    
    public void emitGetCountingNotProccessed(int limit, SocketReplyCallback callback) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.GET_CONTAGEM_CLASSIFICACAO);
                if(limit > 0)
                    obj.put("limit", limit);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitGetCountingNotProccessed()", ex);
            }
            emit(obj, callback);
        }
    }
    
    public void emitSetDatabaseStatus(boolean status, SocketReplyCallback callback) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.SET_DATABASE_STATUS);
                obj.put("status", status);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitSetDatabaseStatus()", ex);
            }
            emit(obj, callback);
        }
    }
    
    public void emitSetTimerOff(int newTimer) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.SET_TIMER_OFF);
                obj.put("timer", newTimer);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitSetDatabaseStatus()", ex);
            }
            emit(obj);
        }
    }
    
    
    public void emitGetTiposRoupaTag(String tag, SocketReplyCallback callback) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.GET_TIPOS_ROUPA_TAG);
                obj.put("tag", tag);
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitSetDatabaseStatus()", ex);
            }
            emit(obj, callback);
        }
    }
    
    public void emitGetLeiturasTagDatas(LeituraTagsDatas leitura, SocketReplyCallback callback) {
        if (connected()) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("code", SocketCode.GET_LEITURAS_TAGS_DATAS);
                obj.put("leitura", new Gson().toJson(leitura));
            } catch (JSONException ex) {
                LOG.fatal("JSONException emitSetDatabaseStatus()", ex);
            }
            emit(obj, callback);
        }
    }

}

class Response {

    private boolean response = false;

    public boolean hasResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

}
