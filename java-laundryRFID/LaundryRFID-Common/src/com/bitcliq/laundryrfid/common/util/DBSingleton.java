/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.util;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author andre
 */
public class DBSingleton {
    
    private static Connection connection = null; // conexão a BD
    public static DBSingleton INSTANCE = null;
    
    private DBSingleton() {

    }

    public static DBSingleton getInstance() {
        if(INSTANCE == null){
            INSTANCE = new DBSingleton();
            connection = new DBConnection().getConnection(); // inicia a conexão a BD
        }
        return INSTANCE;
    }
    
    public Connection getConnection(){ // conexão a BD
        try {
            if(connection != null)
                connection.setAutoCommit(true);
        } catch (SQLException ex) {
            GeneralUtils.getLogger().fatal("SQLException", ex);
        }
        return connection;
    }
}
