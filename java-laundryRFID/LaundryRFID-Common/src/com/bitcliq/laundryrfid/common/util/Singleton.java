/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.util;

import com.bitcliq.laundryrfid.common.listener.CountingListener;
import com.bitcliq.laundryrfid.common.listener.TagListener;
import com.bitcliq.laundryrfid.common.models.Config;
import com.bitcliq.laundryrfid.common.models.MockupReadingTypes;
import com.bitcliq.laundryrfid.common.listener.ISocketEvent;


/**
 *
 * @author andre
 */
public class Singleton {
    
    private static Singleton INSTANCE = null;
    private boolean mockRFIDreader = false; // Leituras em MOCK ou Hardware
    private TagListener tagListener; // listener para tags novas lidas
    private CountingListener countingListener; // listener para a contagem de tags
    private ISocketEvent socketMessageListener; // listener para as mensagens recebidas por socket
    
    private static Config config = null;
    private static final String version = "1.2";
    

    private Singleton() {

    }

    public static Singleton getInstance() {
        if(INSTANCE == null){
            INSTANCE = new Singleton();
            config = Config.load(); // faz load das configurações
        }
        return INSTANCE;
    }

    public boolean isMockRFIDreader() { // se as leituras é para utilizar o mockup
        return mockRFIDreader;
    }

    public void setMockRFIDreader(boolean mockRFIDreader) {
        this.mockRFIDreader = mockRFIDreader;
    }

    public TagListener getTagListener() { // listener para leituras de tags novas
        return tagListener;
    }

    public void setTagListener(TagListener tagListener) {
        this.tagListener = tagListener;
    }

    public CountingListener getCountingListener() { // listener para a alteração de número de peças contadas
        return countingListener;
    }

    public void setCountingListener(CountingListener countingListener) {
        this.countingListener = countingListener;
    }

    public ISocketEvent getSocketMessageListener() {
        return socketMessageListener;
    }

    public void setSocketMessageListener(ISocketEvent socketMessageListener) {
        this.socketMessageListener = socketMessageListener;
    }

    // tipo de leituras de mockup 
    private int mockupReadingType = MockupReadingTypes.MK_TAGS_REGISTADAS_TOTALIDADE;
    
    public int getMockupReadingType() {
        return mockupReadingType;
    }

    public void setMockupReadingType(int mockupReadingType) {
        this.mockupReadingType = mockupReadingType;
    }
    
    public Config getConfig(){
        return config;
    }
    
    public Config reloadConfig(){
        config = Config.load();
        return config;
    }

    public String getVersion() {
        return version;
    }
    
    
    
    
    
    
    
    
}
