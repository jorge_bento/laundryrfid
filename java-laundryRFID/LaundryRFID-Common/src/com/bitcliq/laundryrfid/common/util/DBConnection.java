/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.util;

import com.bitcliq.laundryrfid.common.models.Config;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author andre
 */
public class DBConnection {
    
    private Connection con = null;
    
    public Connection getConnection(){
        
        Config config = Singleton.getInstance().getConfig();
        
        String host = config.getHost();
        String user = config.getUser();
        String password = config.getPassword();
        String database = config.getDatabase();
        String instance = config.getInstance();
        
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            if(host != null && !host.isEmpty()){
                //con = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, user, password);
                con = DriverManager.getConnection("jdbc:sqlserver://"+host+"\\"+instance+";databaseName="+database, user, password);
               
            }
                
          
        } catch (Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Não foi possível conectar ao servidor de base de dados.", "Erro", 0);
        }
        return con;
    }
    
    public void closeConnection(){
        try{
            con.close();
        } catch(Exception e){
            GeneralUtils.getLogger().fatal("JSONException closeConnection().", e);
        }
    }
    
}
