/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.Singleton;

/**
 *
 * @author andre
 */
public class Lote {
    
    public static final String TABLE = Singleton.getInstance().getConfig().getDatabase() + ".dbo.LOTE";
    
    public static final String F_LOTE_ID = "lote_id";
    public static final String F_FABRICANTE = "fabricante";
    public static final String F_REF_ENCOMENDA = "ref_encomenda";
    
    public static final String TF_LOTE_ID = TABLE + "." + F_LOTE_ID;
    public static final String TF_FABRICANTE = TABLE + "." + F_FABRICANTE;
    public static final String TF_REF_ENCOMENDA = TABLE + "." + F_REF_ENCOMENDA;
    
    private int loteID;
    private String fabricante;
    private String refEncomenda;

    public Lote() {
        
    }

    public Lote(String fabricante, String refEncomenda) {
        this.fabricante = fabricante;
        this.refEncomenda = refEncomenda;
    }

    public int getLoteID() {
        return loteID;
    }

    public void setLoteID(int loteID) {
        this.loteID = loteID;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getRefEncomenda() {
        return refEncomenda;
    }

    public void setRefEncomenda(String refEncomenda) {
        this.refEncomenda = refEncomenda;
    }
    
    
    
}
