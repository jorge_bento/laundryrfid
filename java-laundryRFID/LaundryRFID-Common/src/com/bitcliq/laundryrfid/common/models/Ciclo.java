/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.Singleton;
import java.util.Date;

/**
 *
 * @author andre
 */
public class Ciclo {
    
    public static final String TABLE = Singleton.getInstance().getConfig().getDatabase() + ".dbo.CICLO";
    
    public static final String F_CICLO_ID = "ciclo_id";
    public static final String F_END_DATE = "end_date";
    
    public static final String TF_CICLO_ID = TABLE + "." + F_CICLO_ID;
    public static final String TF_END_DATE = TABLE + "." + F_END_DATE;
    
    private int cicloID;
    private Date endDate;

    public Ciclo() {
    }

    public Ciclo(int cicloID, Date endDate) {
        this.cicloID = cicloID;
        this.endDate = endDate;
    }

    public int getCicloID() {
        return cicloID;
    }

    public void setCicloID(int cicloID) {
        this.cicloID = cicloID;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    
    
    
    
}
