/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.Singleton;
import java.util.Date;

/**
 *
 * @author andre
 */
public class AssociacaoTagTipoRoupa {
    
    public static final String TABLE = "[" + Singleton.getInstance().getConfig().getDatabase() + "].[dbo].[ASSOCIACAO_TAG_TIPO_ROUPA]";
    
    public static final String F_DATA = "data";
    public static final String F_TAG_ID = "tag_id";
    public static final String F_TIPO_ROUPA_ID = "tipo_roupa_id";
    
    public static final String TF_DATA = TABLE + "." + F_DATA;
    public static final String TF_TAG_ID = TABLE + "." + F_TAG_ID;
    public static final String TF_TIPO_ROUPA_ID = TABLE + "." + F_TIPO_ROUPA_ID;
    
    private Date timestamp;
    private String tagID;
    private int tipoRoupaID;
    private Date timestampEnd;
    private TipoRoupa tipoRoupa;
    
    

    public Date getTimestamp() {
        return timestamp;
    }
    
    public long getTime() {
        return timestamp != null ? timestamp.getTime() : -1;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getTagID() {
        return tagID;
    }

    public void setTagID(String tagID) {
        this.tagID = tagID;
    }

    public int getTipoRoupaID() {
        return tipoRoupaID;
    }

    public void setTipoRoupaID(int tipoRoupaID) {
        this.tipoRoupaID = tipoRoupaID;
    }

    public Date getTimestampEnd() {
        return timestampEnd;
    }
    
    public long getTimeEnd() {
        return timestampEnd != null ? timestampEnd.getTime() : -1;
    }

    public void setTimestampEnd(Date timestampEnd) {
        this.timestampEnd = timestampEnd;
    }

    public TipoRoupa getTipoRoupa() {
        return tipoRoupa;
    }

    public void setTipoRoupa(TipoRoupa tipoRoupa) {
        this.tipoRoupa = tipoRoupa;
    }
    
    
    
    
    
    
    
}
