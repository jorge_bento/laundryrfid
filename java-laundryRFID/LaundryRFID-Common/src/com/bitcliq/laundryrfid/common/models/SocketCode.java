/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

/**
 *
 * @author andre
 */
public class SocketCode {
    
    // CÓDIGO NÃO PODE SER -1 OU IRÁ SER IGNORADO EM TODO O LADO
    
    // quando o contador global é atualizado
    public static final int UPDATED_COUNTER = 1;
    
    // quando o contador por peça é atualizado
    public static final int UPDATED_PIECES_COUNTER = 2;
    
    // quando o timer de leitura de tags muda
    public static final int UPDATED_TIMER = 3;
    
    // quando a mensagem é uma resposta positiva
    public static final int REPLY_YES = 4;
    
    // quando a mensagem é uma resposta negativa
    public static final int REPLY_NO = 5;
    
    // iniciar as leituras (vai apagar a lista de classificações)
    public static final int START_READINGS = 6;
    
    // parar as leituras
    public static final int END_READINGS = 7;
    
    // pergunta se leitura atual é mock ou não
    public static final int IS_MOCK_READING = 8;
    
    // define a leitura para mock
    public static final int SET_MOCK_READING = 9;
    
    // define o tipo de leitura mockup
    public static final int SET_MOCK_READING_TYPE = 10;
    
    // reinicia o leitor mockup com novo tipo de leitura
    public static final int RESTART_MOCK_READING_W_TYPE = 11;
    
    // limpa as tags que foram lidas
    public static final int CLEAR_READ_TAGS = 12;
    
    // pedido para obter a lista de tipos de roupa
    public static final int GET_TYPES_CLOTHING = 13;
    
    // lista de tipos de roupa
    public static final int LIST_TYPES_CLOTHING = 14;
    
    // registar as tags com dados do fornecedor
    public static final int REGISTER_TAGS_PROVIDER = 15;
    
    // registar o ciclo das tags com tipo de roupa
    public static final int REGISTER_TAGS_CLOTHING = 16;
    
    // pedir a quantidade de tags lidas
    public static final int GET_COUNT_READ_TAGS = 17;
    
    // fazer um ping ao serviço
    public static final int PING = 18;
    
    // pedido de informações do serviço
    public static final int GET_STATUS = 19;
    
    // pedido das contagens não processadas
    public static final int GET_CONTAGEM_CLASSIFICACAO = 20;
    
    // obter as informações iniciais para a interface gráfica
    public static final int GET_STARTUP_SETTINGS = 21;
    
    // definir o estado da base de dados (a guardar ou não)
    public static final int SET_DATABASE_STATUS = 22;
    
    // definir o temporizador temporário
    public static final int SET_TIMER_OFF = 23;
    
    // limpar a base de dados
    public static final int CLEAR_DATABASE = 24;
    
    // obter a lista das tags que foram lidas
    public static final int GET_TAGS_READ = 25;
    
    // obter a lista completa de tipos de roupa de uma tag
    public static final int GET_TIPOS_ROUPA_TAG = 26;
    
    // obter as leituras de uma tag entre datas
    public static final int GET_LEITURAS_TAGS_DATAS = 27;
    
    // inicia a leitura (não limpa a contagem
    public static final int CONTINUE_READINGS = 28;
    
    
}
