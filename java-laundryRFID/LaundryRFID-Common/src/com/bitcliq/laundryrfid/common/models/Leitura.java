/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.GeneralUtils;
import com.bitcliq.laundryrfid.common.util.Singleton;
import java.util.Date;

/**
 *
 * @author andre
 */
public class Leitura {
    
    public static final String TABLE = Singleton.getInstance().getConfig().getDatabase() + ".dbo.LEITURA";
    
    public static final String F_DATA = "data";
    public static final String F_TAG_ID = "tag_id";
    public static final String F_ENTRADA_OU_SAIDA = "entrada_ou_saida";
    
    public static final String TF_DATA = TABLE + "." + F_DATA;
    public static final String TF_TAG_ID = TABLE + "." + F_TAG_ID;
    public static final String TF_ENTRADA_OU_SAIDA = TABLE + "." + F_ENTRADA_OU_SAIDA;
    
    private Date timestamp;
    private String tagID;
    private boolean entradaOuSaida;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setTimestamp() {
        this.timestamp = GeneralUtils.getNow();
    }

    public String getTagID() {
        return tagID;
    }

    public void setTagID(String tagID) {
        this.tagID = tagID;
    }
    
    public boolean isEntrada(){
        return entradaOuSaida;
    }
    
    public boolean isSaida(){
        return !entradaOuSaida;
    }
    
    public void setEntrada(){
        this.entradaOuSaida = true;
    }
    
    public void setSaida(){
        this.entradaOuSaida = false;
    }
    
    public int getEntradaSaidaInt(){
        return entradaOuSaida ? 1 : 0;
    }
    
    public void setEntradaSaidaInt(int entradaOuSaida){
        this.entradaOuSaida = entradaOuSaida == 1;
    }
    
    public void setEntradaSaidaInt(boolean entradaOuSaida){
        this.entradaOuSaida = entradaOuSaida;
    }
    
    
    
}
