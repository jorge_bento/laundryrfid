/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.Singleton;

/**
 *
 * @author andre
 */
public class ContagemClassificacao {
    
    public static final String TABLE = Singleton.getInstance().getConfig().getDatabase() + ".dbo.CONTAGEM_CLASSIFICACAO";
    
    public static final String F_CICLO_ID = "ciclo_id";
    public static final String F_CONTADOR = "contador";
    public static final String F_TIPO_ROUPA_ID = "tipo_roupa_id";
    public static final String F_PROCESSADO = "processado";
    
    public static final String TF_CICLO_ID = TABLE + "." + F_CICLO_ID;
    public static final String TF_CONTADOR = TABLE + "." + F_CONTADOR;
    public static final String TF_TIPO_ROUPA_ID = TABLE + "." + F_TIPO_ROUPA_ID;
    public static final String TF_PROCESSADO = TABLE + "." + F_PROCESSADO;
    
    private int cicloID;
    private int contador;
    private int tipoRoupaID;
    private TipoRoupa tipoRoupa;
    private boolean processado;

    public int getCicloID() {
        return cicloID;
    }

    public void setCicloID(int cicloID) {
        this.cicloID = cicloID;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public int getTipoRoupaID() {
        return tipoRoupaID;
    }

    public void setTipoRoupaID(int tipoRoupaID) {
        this.tipoRoupaID = tipoRoupaID;
    }

    public TipoRoupa getTipoRoupa() {
        return tipoRoupa;
    }

    public void setTipoRoupa(TipoRoupa tipoRoupa) {
        this.tipoRoupa = tipoRoupa;
    }

    public boolean isProcessado() {
        return processado;
    }

    public void setProcessado(boolean processado) {
        this.processado = processado;
    }
    
    public int isProcessadoInt(){
        return processado ? 1 : 0;
    }
    
    public void setProcessadoInt(int processado) {
        this.processado = processado == 1;
    }
    
    public void incrementCounter(){
        this.contador++;
    }
    
    
    
}
