/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

/**
 *
 * @author andre
 */
public class MockupReadingTypes {
    // Tags que estão registadas na base de dados com todos os dados (Tipo de Roupa e Fornecedor)
    public static final int MK_TAGS_REGISTADAS_TOTALIDADE = 1;
    
    // Tags que estão registadas na base de dados apenas com os dados do Fornecedor
    public static final int MK_TAGS_REGISTADAS_FORNECEDOR = 2;
    
    // Tags que não estão registadas na base de dados
    public static final int MK_TAGS_NAO_REGISTADAS = 3;
    
}
