/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;


import com.bitcliq.laundryrfid.common.util.GeneralUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author andre
 */
public class SocketMessage {
    
    private int code = -1;
    private JSONObject message;
    private boolean needsReply = false;
    
    public SocketMessage() {
        
    }

    public SocketMessage(int code, JSONObject message, boolean needsReply) {
        this.code = code;
        this.message = message;
        this.needsReply = needsReply;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public JSONObject getMessage() {
        return message;
    }

    public void setMessage(JSONObject message) {
        this.message = message;
    }

    public boolean needsReply() {
        return needsReply;
    }

    public void setNeedsReply(boolean needsReply) {
        this.needsReply = needsReply;
    }
    
    public void editMessage(String code, Object value) {
        try {
            message.put(code, value);
        } catch (JSONException ex) {
            GeneralUtils.getLogger().fatal("JSONException editMessage(). Message: ("+this.message.toString()+") ", ex);
        }
    }
    
    public boolean isYes(){
        return this.code == SocketCode.REPLY_YES;
    }
    
    
    
}

