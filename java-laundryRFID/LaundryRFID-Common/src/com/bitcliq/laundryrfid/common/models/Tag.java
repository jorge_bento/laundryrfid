/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.Singleton;

/**
 *
 * @author andre
 */
public class Tag {
    
    public static final String TABLE = Singleton.getInstance().getConfig().getDatabase() + ".dbo.TAGS";
    
    public static final String F_TAG_ID = "tag_id";
    public static final String F_LOTE_ID = "lote_id";
    
    public static final String TF_TAG_ID = TABLE + "." + F_TAG_ID;
    public static final String TF_LOTE_ID = TABLE + "." + F_LOTE_ID;
    
    private String tagID;
    private int loteID;
    private TipoRoupa ultimoTipoRoupa;
    private Lote lote;

    public String getTagID() {
        return tagID;
    }

    public void setTagID(String tagID) {
        this.tagID = tagID;
    }
    
    public int getLoteID() {
        return loteID;
    }

    public void setLoteID(int loteID) {
        this.loteID = loteID;
    }

    public TipoRoupa getUltimoTipoRoupa() {
        return ultimoTipoRoupa;
    }

    public void setUltimoTipoRoupa(TipoRoupa ultimoTipoRoupa) {
        if(ultimoTipoRoupa.getNome() != null && ultimoTipoRoupa.getTipoRoupaID() != 0)
            this.ultimoTipoRoupa = ultimoTipoRoupa;
        else 
            this.ultimoTipoRoupa = null;
    }

    public Lote getLote() {
        return lote;
    }

    public void setLote(Lote lote) {
        if(lote.getFabricante() != null && lote.getRefEncomenda() != null && lote.getLoteID() != 0)
            this.lote = lote;
        else 
            this.lote = null;
    }
    
    
    
    
    
    
    
}
