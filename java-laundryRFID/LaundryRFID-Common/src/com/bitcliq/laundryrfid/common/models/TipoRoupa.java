/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.Singleton;

/**
 *
 * @author andre
 */
public class TipoRoupa {
    
    public static final String TABLE = Singleton.getInstance().getConfig().getDatabase() + ".dbo.TIPO_ROUPA";
    
    public static final String F_TIPO_ROUPA_ID = "tipo_roupa_id";
    public static final String F_NOME = "nome";
    public static final String F_REF_PRIMAVERA = "refPrimavera";
    
    public static final String TF_TIPO_ROUPA_ID = TABLE + "." + F_TIPO_ROUPA_ID;
    public static final String TF_NOME = TABLE + "." + F_NOME;
    public static final String TF_REF_PRIMAVERA = TABLE  + "." + F_REF_PRIMAVERA;
    
    private int tipoRoupaID;
    private String nome;
    private String refPrimavera;

    public TipoRoupa() {
    }

    public TipoRoupa(int tipoRoupaID, String nome, String refPrimavera) {
        this.tipoRoupaID = tipoRoupaID;
        this.nome = nome;
        this.refPrimavera = refPrimavera;
    }

    public int getTipoRoupaID() {
        return tipoRoupaID;
    }

    public void setTipoRoupaID(int tipoRoupaID) {
        this.tipoRoupaID = tipoRoupaID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRefPrimavera() {
        return refPrimavera;
    }

    public void setRefPrimavera(String refPrimavera) {
        this.refPrimavera = refPrimavera;
    }
    

    @Override
    public String toString() {
        return nome;
    }
    
    
}
