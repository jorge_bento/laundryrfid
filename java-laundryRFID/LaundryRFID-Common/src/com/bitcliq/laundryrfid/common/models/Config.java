/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.common.models;

import com.bitcliq.laundryrfid.common.util.GeneralUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 *
 * @author andre
 */
public class Config {
    
    // database
    private String host = "localhost";
    private String instance = "SQLEXPRESS";
    private String user = "sa";
    private String password = "123456";
    private String database = "Tags";
    
    // timer
    private int timer = 20;
    
    // socket
    private String socketLocation = "http://localhost:8888";
    
    // usb
    private String serialPort = "COM1";
    private int boudRate = 115200;
    
    // autenticacao
    private int autenticacaoExpira = 10;
    
    public static Config load(){
        Config config = new Config();
        
        try {
            String pathConfig = "";
            try {
                File fc = new File(Config.class.getProtectionDomain().getCodeSource().getLocation().toURI());
                pathConfig = fc.getParent();
            } catch (URISyntaxException ex) {
                GeneralUtils.getLogger().fatal("URISyntaxException", ex);
            }
            
            File f = new File(pathConfig + "/config.txt");
            if(f.exists() && !f.isDirectory()){
                Properties prop = new Properties();
                prop.load(new FileReader(pathConfig + "/config.txt"));

                config.host = prop.getProperty("host", config.host);
                config.instance = prop.getProperty("instance", config.instance);
                config.user = prop.getProperty("user", config.user);
                config.password = prop.getProperty("password", config.password);
                config.database = prop.getProperty("database", config.database);

                config.timer = Integer.parseInt(prop.getProperty("timer", config.timer + ""));
                
                config.socketLocation = prop.getProperty("socketLocation", config.socketLocation);
                
                config.serialPort = prop.getProperty("serialPort", config.serialPort);
                config.boudRate = Integer.parseInt(prop.getProperty("boudRate", config.boudRate + ""));
                
                config.autenticacaoExpira = Integer.parseInt(prop.getProperty("autenticacaoExpira", config.autenticacaoExpira + ""));
                
            } else {
                String cfg = "# Quantidade de tempo a espera antes de dar término à contagem (segundos)\n" +
                        "timer = " + config.timer + "\n\n" +
                        "# conexão à BD\n" +
                        "host = " + config.host + "\n" +
                        "instance = " + config.instance + "\n" +
                        "user = " + config.user + "\n" +
                        "password = " + config.password + "\n" +
                        "database = " + config.database + "\n\n" +
                        "#sockets\n" +
                        "socketLocation = " + config.socketLocation + "\n\n" +
                        "#reader\n" +
                        "serialPort = " + config.serialPort + "\n" +
                        "boudRate = " + config.boudRate + "\n\n" +
                        "#autenticacao\n" + 
                        "autenticacaoExpira = " + config.autenticacaoExpira + "\n";
                
                
                PrintWriter writer = new PrintWriter(pathConfig + "/config.txt", "UTF-8");
                writer.print(cfg);
                writer.close();

            }
            
        } catch (FileNotFoundException ex) {
            GeneralUtils.getLogger().fatal("FileNotFoundException", ex);
        } catch (IOException ex) {
            GeneralUtils.getLogger().fatal("IOException", ex);
        }
        
        return config;
    }

    public String getHost() {
        return host;
    }

    public String getInstance() {
        return instance;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getDatabase() {
        return database;
    }
    

    public int getTimer() {
        return timer;
    }

    
    public String getSocketLocation() {
        return socketLocation;
    }
    

    public String getSerialPort() {
        return serialPort;
    }

    public int getBoudRate() {
        return boudRate;
    }
    

    public int getAutenticacaoExpira() {
        return autenticacaoExpira;
    }
    
    
    
}
