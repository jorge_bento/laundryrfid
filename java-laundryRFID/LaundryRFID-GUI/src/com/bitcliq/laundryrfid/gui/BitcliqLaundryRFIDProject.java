/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.gui;

import com.bitcliq.laundryrfid.common.models.AssociacaoTagTipoRoupa;
import com.bitcliq.laundryrfid.common.models.ContagemClassificacao;
import com.bitcliq.laundryrfid.common.models.Leitura;
import com.bitcliq.laundryrfid.common.models.LeituraTagsDatas;
import com.bitcliq.laundryrfid.common.models.Lote;
import com.bitcliq.laundryrfid.common.models.MockupReadingTypes;
import com.bitcliq.laundryrfid.common.models.SocketCode;
import com.bitcliq.laundryrfid.common.models.SocketMessage;
import com.bitcliq.laundryrfid.common.models.Tag;
import com.bitcliq.laundryrfid.common.models.TipoRoupa;
import com.bitcliq.laundryrfid.common.util.GeneralUtils;
import com.bitcliq.laundryrfid.common.util.Singleton;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;

/**
 *
 * @author andre
 */
public class BitcliqLaundryRFIDProject extends javax.swing.JFrame {

    private int counter = 0; // contador de peças
    private SocketConnection socket = null;
    
    private DefaultListModel ESListModelTags, ESListModelRoupas, ESListModelLeituras;

    private ArrayList<TipoRoupa> roupas = new ArrayList<>(); //dbRepo.getTiposDeRoupa();
    private ArrayList<AssociacaoTagTipoRoupa> assocTipoRoupas = new ArrayList<>();
    private final static Logger LOG = GeneralUtils.getLogger();
    private static ArrayList<String> tags = new ArrayList<>();
    private static final Gson GSON = new Gson();
    private static final String PASSWORD = "a62eb7ba6443d78d3270239d71a493fd";
    private static long passwordExpires = 0;
    private static final int minutesToExpire = Singleton.getInstance().getConfig().getAutenticacaoExpira();
    
    
    
    
    private boolean isSavingDatabase = true;

    /**
     * Creates new form GUI
     */
    public BitcliqLaundryRFIDProject() {
        initComponents();
        this.setTitle(this.getTitle() + " v" + Singleton.getInstance().getVersion());
        
        ESListModelTags = (DefaultListModel) ESListaTags.getModel();
        ESListModelRoupas = (DefaultListModel) ESListaTiposRoupa.getModel();
        ESListModelLeituras = (DefaultListModel) ESListaLeituras.getModel();
        
        ESFillListTags(null);

        socket = new SocketConnection();

        socket.onConnect(() -> {
            socket.on("message", (SocketMessage message) -> {
                try {

                    if (message.getCode() == SocketCode.UPDATED_PIECES_COUNTER) {
                        // sempre que a contagem com classificação (tipo de roupa - contagem) é alterada

                        java.lang.reflect.Type type = new TypeToken<HashMap<Integer, ContagemClassificacao>>() {
                        }.getType();

                        HashMap<Integer, ContagemClassificacao> contagem = GSON.fromJson(message.getMessage().getString("count"), type);

                        String msg = "";
                        for (Map.Entry<Integer, ContagemClassificacao> value : contagem.entrySet()) {
                            ContagemClassificacao classificacao = value.getValue();
                            msg = msg.concat(classificacao.getTipoRoupa().getNome() + " - " + classificacao.getContador() + " peça" + (classificacao.getContador() != 1 ? "s" : "") + "\n");
                        }
                        CTListaContagem.setText(msg);
                        CTProgressBar.setValue(0);

                    } else if (message.getCode() == SocketCode.UPDATED_TIMER) {
                        // quando o timer for alterado
                        int timerOff = message.getMessage().getInt("limit");
                        int piece = 100 / timerOff;
                        CTProgressBar.setValue(message.getMessage().getInt("timer") * piece);

                    } else if (message.getCode() == SocketCode.UPDATED_COUNTER) {
                        // quando o contador de peças contadas for alterado

                        //int totalCount = message.getMessage().getInt("count");
                        
                        int comRoupa = message.getMessage().getInt("comRoupa");
                        int semRoupa = message.getMessage().getInt("semRoupa");
                        int total = comRoupa + semRoupa;
                        
                        
                        counter = total;
                        SRTagsContadas.setText(total + "");
                        STTagsContadas.setText(total + "");
                        CTTagsContadas.setText(total + "");
                        CTTagsContadasComRoupa.setText(comRoupa + "");
                        CTTagsContadasSemRoupa.setText(semRoupa + "");

                    }

                } catch (JSONException ex) {
                    LOG.fatal("JSONException parseMessage. Message: ("+message.getMessage().toString()+") ", ex);
                }
            });

            initialStartup();
        });
        
        socket.connect();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        guiTabsPannel = new javax.swing.JTabbedPane();
        ContagemPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        CTListaContagem = new javax.swing.JTextArea();
        SetupTiposRoupaPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        SRTagsContadas = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        SRComboBoxRoupa = new javax.swing.JComboBox<>();
        SRSetupButton = new javax.swing.JButton();
        SetupTagsFornecedorPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        STTagsContadas = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        STNomeFornecedor = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        STRefEncomenda = new javax.swing.JTextField();
        STSetupButton = new javax.swing.JButton();
        MockupPanel = new javax.swing.JPanel();
        MKToggleButton = new javax.swing.JToggleButton();
        jLabel4 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        MKDefinicoesComboBox = new javax.swing.JComboBox<>();
        MKReiniciarLeitura = new javax.swing.JButton();
        MKIniciarButton = new javax.swing.JButton();
        MKPararButton = new javax.swing.JButton();
        EstatisticasPanel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        ESListaTags = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        ESListaTiposRoupa = new javax.swing.JList<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        ESListaLeituras = new javax.swing.JList<>();
        ESAtualizarTags = new javax.swing.JButton();
        ConfiguracoesPanel = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        CFTemporizador = new javax.swing.JTextField();
        CFSaveTemporizador = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        CFGuardarPrimavera = new javax.swing.JToggleButton();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        CFLimparBaseDeDados = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        CTStop = new javax.swing.JButton();
        CTContinuar = new javax.swing.JButton();
        CTProgressBar = new javax.swing.JProgressBar();
        CTStart = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        CTTagsContadas = new javax.swing.JLabel();
        CTTagsContadasSemRoupa = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        CTTagsContadasComRoupa = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bitcliq - LaundryRFID Project");
        setMinimumSize(new java.awt.Dimension(1297, 567));

        guiTabsPannel.setFont(new java.awt.Font("Calibri", 0, 26)); // NOI18N
        guiTabsPannel.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                guiTabsPannelStateChanged(evt);
            }
        });

        ContagemPanel.setName("Contagem"); // NOI18N

        jScrollPane1.setEnabled(false);

        CTListaContagem.setEditable(false);
        CTListaContagem.setColumns(20);
        CTListaContagem.setFont(new java.awt.Font("Calibri", 0, 40)); // NOI18N
        CTListaContagem.setRows(5);
        jScrollPane1.setViewportView(CTListaContagem);

        javax.swing.GroupLayout ContagemPanelLayout = new javax.swing.GroupLayout(ContagemPanel);
        ContagemPanel.setLayout(ContagemPanelLayout);
        ContagemPanelLayout.setHorizontalGroup(
            ContagemPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ContagemPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1272, Short.MAX_VALUE)
                .addContainerGap())
        );
        ContagemPanelLayout.setVerticalGroup(
            ContagemPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ContagemPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
        );

        guiTabsPannel.addTab("Contagem", ContagemPanel);

        SetupTiposRoupaPanel.setName("SetupTiposRoupa"); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Definir um novo Tipo de Roupa", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Calibri", 1, 24))); // NOI18N
        jPanel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel1.setText("Tags Contabilizadas:");

        SRTagsContadas.setFont(new java.awt.Font("Calibri", 1, 30)); // NOI18N
        SRTagsContadas.setText("0");

        jLabel3.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel3.setText("Novo Tipo de Roupa:");

        SRComboBoxRoupa.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        SRComboBoxRoupa.setMaximumRowCount(20);
        SRComboBoxRoupa.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                SRComboBoxRoupaPopupMenuWillBecomeVisible(evt);
            }
        });

        SRSetupButton.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        SRSetupButton.setText("Definir");
        SRSetupButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SRSetupButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SRComboBoxRoupa, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(SRTagsContadas)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 246, Short.MAX_VALUE))
                    .addComponent(SRSetupButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(SRTagsContadas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SRComboBoxRoupa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(SRSetupButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout SetupTiposRoupaPanelLayout = new javax.swing.GroupLayout(SetupTiposRoupaPanel);
        SetupTiposRoupaPanel.setLayout(SetupTiposRoupaPanelLayout);
        SetupTiposRoupaPanelLayout.setHorizontalGroup(
            SetupTiposRoupaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SetupTiposRoupaPanelLayout.createSequentialGroup()
                .addContainerGap(400, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(403, Short.MAX_VALUE))
        );
        SetupTiposRoupaPanelLayout.setVerticalGroup(
            SetupTiposRoupaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SetupTiposRoupaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(157, Short.MAX_VALUE))
        );

        guiTabsPannel.addTab("Setup de Tipos de Roupa", SetupTiposRoupaPanel);

        SetupTagsFornecedorPanel.setName("SetupTagsFornecedor"); // NOI18N

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Definir o Fornecedor das Tags", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Calibri", 1, 24))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel2.setText("Tags Contabilizadas:");

        STTagsContadas.setFont(new java.awt.Font("Calibri", 1, 30)); // NOI18N
        STTagsContadas.setText("0");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detalhes do Fornecedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Calibri", 1, 24))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel5.setText("Nome do Fornecedor:");

        STNomeFornecedor.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel6.setText("Referência de Encomenda:");

        STRefEncomenda.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(STNomeFornecedor))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(STRefEncomenda, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(STNomeFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(STRefEncomenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        STSetupButton.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        STSetupButton.setText("Definir");
        STSetupButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                STSetupButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(STTagsContadas)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(STSetupButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(STTagsContadas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(STSetupButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout SetupTagsFornecedorPanelLayout = new javax.swing.GroupLayout(SetupTagsFornecedorPanel);
        SetupTagsFornecedorPanel.setLayout(SetupTagsFornecedorPanelLayout);
        SetupTagsFornecedorPanelLayout.setHorizontalGroup(
            SetupTagsFornecedorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SetupTagsFornecedorPanelLayout.createSequentialGroup()
                .addContainerGap(373, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(375, Short.MAX_VALUE))
        );
        SetupTagsFornecedorPanelLayout.setVerticalGroup(
            SetupTagsFornecedorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SetupTagsFornecedorPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        guiTabsPannel.addTab("Setup Tags (Fornecedor)", SetupTagsFornecedorPanel);

        MockupPanel.setName("MockupPanel"); // NOI18N

        MKToggleButton.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        MKToggleButton.setText("Hardware");
        MKToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MKToggleButtonActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel4.setText("Tipo de Leituras:");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Definições de Leituras Mockup", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 24))); // NOI18N

        jLabel7.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel7.setText("Tags a enviar:");

        MKDefinicoesComboBox.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        MKDefinicoesComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tags com Tipo de Roupa", "Tags com Fornecedor", "Tags não registadas na BD" }));

        MKReiniciarLeitura.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        MKReiniciarLeitura.setText("Reiniciar Leitura");
        MKReiniciarLeitura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MKReiniciarLeituraActionPerformed(evt);
            }
        });

        MKIniciarButton.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        MKIniciarButton.setText("Iniciar");
        MKIniciarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MKIniciarButtonActionPerformed(evt);
            }
        });

        MKPararButton.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        MKPararButton.setText("Parar");
        MKPararButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MKPararButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MKDefinicoesComboBox, 0, 364, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(MKIniciarButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MKPararButton)
                        .addGap(18, 18, 18)
                        .addComponent(MKReiniciarLeitura)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(MKDefinicoesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(MKReiniciarLeitura)
                    .addComponent(MKIniciarButton)
                    .addComponent(MKPararButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout MockupPanelLayout = new javax.swing.GroupLayout(MockupPanel);
        MockupPanel.setLayout(MockupPanelLayout);
        MockupPanelLayout.setHorizontalGroup(
            MockupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MockupPanelLayout.createSequentialGroup()
                .addContainerGap(377, Short.MAX_VALUE)
                .addGroup(MockupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MockupPanelLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(MKToggleButton)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MockupPanelLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(373, Short.MAX_VALUE))))
        );
        MockupPanelLayout.setVerticalGroup(
            MockupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MockupPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MockupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(MKToggleButton)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(216, Short.MAX_VALUE))
        );

        guiTabsPannel.addTab("Mockup", MockupPanel);

        EstatisticasPanel.setName("EstatisticasPanel"); // NOI18N

        jPanel5.setLayout(new java.awt.GridLayout(1, 3, 10, 0));

        ESListaTags.setFont(new java.awt.Font("Calibri", 0, 20)); // NOI18N
        ESListaTags.setModel(new DefaultListModel());
        ESListaTags.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        ESListaTags.setEnabled(false);
        ESListaTags.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                ESListaTagsValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(ESListaTags);

        jPanel5.add(jScrollPane2);

        ESListaTiposRoupa.setFont(new java.awt.Font("Calibri", 0, 20)); // NOI18N
        ESListaTiposRoupa.setModel(new DefaultListModel());
        ESListaTiposRoupa.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        ESListaTiposRoupa.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                ESListaTiposRoupaValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(ESListaTiposRoupa);

        jPanel5.add(jScrollPane3);

        ESListaLeituras.setFont(new java.awt.Font("Calibri", 0, 20)); // NOI18N
        ESListaLeituras.setModel(new DefaultListModel());
        ESListaLeituras.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane4.setViewportView(ESListaLeituras);

        jPanel5.add(jScrollPane4);

        ESAtualizarTags.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        ESAtualizarTags.setText("Atualizar Lista de Tags Lidas");
        ESAtualizarTags.setToolTipText("");
        ESAtualizarTags.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ESAtualizarTagsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout EstatisticasPanelLayout = new javax.swing.GroupLayout(EstatisticasPanel);
        EstatisticasPanel.setLayout(EstatisticasPanelLayout);
        EstatisticasPanelLayout.setHorizontalGroup(
            EstatisticasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(EstatisticasPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(EstatisticasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 1272, Short.MAX_VALUE)
                    .addGroup(EstatisticasPanelLayout.createSequentialGroup()
                        .addComponent(ESAtualizarTags)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        EstatisticasPanelLayout.setVerticalGroup(
            EstatisticasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(EstatisticasPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ESAtualizarTags)
                .addContainerGap())
        );

        guiTabsPannel.addTab("Estatísticas", EstatisticasPanel);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Configurações", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Calibri", 1, 24))); // NOI18N

        jLabel9.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel9.setText("Temporizador:");

        CFTemporizador.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N

        CFSaveTemporizador.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        CFSaveTemporizador.setText("Guardar");
        CFSaveTemporizador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CFSaveTemporizadorActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel11.setText("Guardar dados Primavera:");

        CFGuardarPrimavera.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        CFGuardarPrimavera.setText("ON");
        CFGuardarPrimavera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CFGuardarPrimaveraActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel12.setText("Base de Dados:");

        CFLimparBaseDeDados.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        CFLimparBaseDeDados.setText("Limpar");
        CFLimparBaseDeDados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CFLimparBaseDeDadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CFGuardarPrimavera, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CFTemporizador))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CFLimparBaseDeDados, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(CFSaveTemporizador, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(CFTemporizador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CFSaveTemporizador)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(CFGuardarPrimavera))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(CFLimparBaseDeDados))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout ConfiguracoesPanelLayout = new javax.swing.GroupLayout(ConfiguracoesPanel);
        ConfiguracoesPanel.setLayout(ConfiguracoesPanelLayout);
        ConfiguracoesPanelLayout.setHorizontalGroup(
            ConfiguracoesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ConfiguracoesPanelLayout.createSequentialGroup()
                .addContainerGap(433, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(422, Short.MAX_VALUE))
        );
        ConfiguracoesPanelLayout.setVerticalGroup(
            ConfiguracoesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConfiguracoesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(139, Short.MAX_VALUE))
        );

        guiTabsPannel.addTab("Configurações", ConfiguracoesPanel);

        CTStop.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        CTStop.setText("Parar");
        CTStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CTStopActionPerformed(evt);
            }
        });

        CTContinuar.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        CTContinuar.setText("Continuar");
        CTContinuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CTContinuarActionPerformed(evt);
            }
        });

        CTStart.setFont(new java.awt.Font("Calibri", 0, 36)); // NOI18N
        CTStart.setText("Iniciar");
        CTStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CTStartActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Calibri", 0, 28)); // NOI18N
        jLabel14.setText("| Contagem Total:");

        CTTagsContadas.setFont(new java.awt.Font("Calibri", 1, 30)); // NOI18N
        CTTagsContadas.setText("0");

        CTTagsContadasSemRoupa.setFont(new java.awt.Font("Calibri", 1, 30)); // NOI18N
        CTTagsContadasSemRoupa.setText("0");

        jLabel10.setFont(new java.awt.Font("Calibri", 0, 28)); // NOI18N
        jLabel10.setText("| Tags sem Tipo Roupa:");

        CTTagsContadasComRoupa.setFont(new java.awt.Font("Calibri", 1, 30)); // NOI18N
        CTTagsContadasComRoupa.setText("0");

        jLabel8.setFont(new java.awt.Font("Calibri", 0, 28)); // NOI18N
        jLabel8.setText("Tags com Tipo Roupa:");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CTProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CTTagsContadasComRoupa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CTTagsContadasSemRoupa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CTTagsContadas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CTStart)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CTContinuar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CTStop)))
                .addGap(6, 6, 6))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(CTProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CTStop)
                    .addComponent(jLabel8)
                    .addComponent(CTTagsContadasComRoupa)
                    .addComponent(jLabel10)
                    .addComponent(CTTagsContadasSemRoupa)
                    .addComponent(jLabel14)
                    .addComponent(CTTagsContadas)
                    .addComponent(CTContinuar)
                    .addComponent(CTStart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(guiTabsPannel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(guiTabsPannel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        guiTabsPannel.getAccessibleContext().setAccessibleName("tab");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SRSetupButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SRSetupButtonActionPerformed
        if (counter <= 0) { // caso não houver tags contadas
            JOptionPane.showMessageDialog(null, "Não foram contabilizadas tags", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (SRComboBoxRoupa.getSelectedIndex() < 0) { // caso não houver um tipo de roupa selecionado
            JOptionPane.showMessageDialog(null, "Selecione um tipo de roupa antes de prosseguir", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        TipoRoupa roupa = roupas.get(SRComboBoxRoupa.getSelectedIndex()); // tipo de roupa selecionado
        socket.emitRegisterClothingInformation(roupa, (message) -> {
            if(message != null && message.isYes()) {
                JOptionPane.showMessageDialog(null, "Todas as tags foram registadas com sucesso.", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Ocorreu um erro ao registar as tags. (Pode ter faltado registar o fornecedor)", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        });
    }//GEN-LAST:event_SRSetupButtonActionPerformed

    private void STSetupButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_STSetupButtonActionPerformed
        if (counter <= 0) { // caso não houver tags contadas
            JOptionPane.showMessageDialog(null, "Não foram contabilizadas tags", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (STNomeFornecedor.getText().trim().length() <= 0) { // caso o campo nome de fornecedor esteja vazio
            JOptionPane.showMessageDialog(null, "Indique o nome do Fornecedor", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (STRefEncomenda.getText().trim().length() <= 0) { // caso o campo referencia de encomenda esteja vazio
            JOptionPane.showMessageDialog(null, "Indique a referência da Encomenda", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }

        Lote lote = new Lote(STNomeFornecedor.getText(), STRefEncomenda.getText());
        socket.emitRegisterProviderInformation(lote, (message) -> {
            if (message != null && message.isYes()) {
                JOptionPane.showMessageDialog(null, "Todas as tags foram registadas com sucesso.", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                STNomeFornecedor.setText("");
                STRefEncomenda.setText("");

            } else {
                String errorMsg = "Ocorreu um erro.";
                try {
                    errorMsg = message.getMessage().has("error") ? message.getMessage().getString("error") : errorMsg;
                } catch (JSONException ex) {
                    LOG.fatal("JSONException STSetupButton(). Message: ("+message.getMessage().toString()+") ", ex);
                }
                
                JOptionPane.showMessageDialog(null, errorMsg, "Erro", JOptionPane.ERROR_MESSAGE);
            }
        });
    }//GEN-LAST:event_STSetupButtonActionPerformed

    private void MKReiniciarLeituraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MKReiniciarLeituraActionPerformed
        socket.emitCode(SocketCode.IS_MOCK_READING, (SocketMessage message) -> {
            if (message != null && message.isYes()) {
                socket.emitCode(SocketCode.END_READINGS, (SocketMessage message2) -> {
                    socket.emitChangeMockupReadingType(mockupReadingType(), (SocketMessage message3) -> {
                        socket.emitCode(SocketCode.START_READINGS, (SocketMessage message4) -> {

                        });
                    });
                });
            } else {
                JOptionPane.showMessageDialog(null, "O sistema não está em modo mockup", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        });
    }//GEN-LAST:event_MKReiniciarLeituraActionPerformed

    private void MKToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MKToggleButtonActionPerformed
        boolean isSelected = MKToggleButton.isSelected(); // troca o método entre leituras de Hardware para Mockup
        MKToggleButton.setText(isSelected ? "Mockup" : "Hardware");
        socket.emitChangeReadingType(isSelected, (message) -> {
            if (isSelected) {
                socket.emitChangeMockupReadingType(mockupReadingType(), null);
            }

            socket.emitClearReadTags((message1) -> {
                socket.emitCode(SocketCode.START_READINGS);
            });

        });
    }//GEN-LAST:event_MKToggleButtonActionPerformed

    private void MKIniciarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MKIniciarButtonActionPerformed
        socket.emitCode(SocketCode.IS_MOCK_READING, (SocketMessage message) -> {
            if (message != null && message.isYes()) {
                socket.emitCode(SocketCode.START_READINGS);
            } else {
                JOptionPane.showMessageDialog(null, "O sistema não está em modo mockup", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        });
    }//GEN-LAST:event_MKIniciarButtonActionPerformed

    private void MKPararButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MKPararButtonActionPerformed
        socket.emitCode(SocketCode.IS_MOCK_READING, (SocketMessage message) -> {
            if (message != null && message.isYes()) {
                socket.emitCode(SocketCode.END_READINGS);
            } else {
                JOptionPane.showMessageDialog(null, "O sistema não está em modo mockup", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        });
    }//GEN-LAST:event_MKPararButtonActionPerformed

    private void SRComboBoxRoupaPopupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_SRComboBoxRoupaPopupMenuWillBecomeVisible
        if (SRComboBoxRoupa.getItemCount() <= 0) {
            initialStartup();
        }
    }//GEN-LAST:event_SRComboBoxRoupaPopupMenuWillBecomeVisible

    private void ESListaTagsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_ESListaTagsValueChanged
        if(evt.getValueIsAdjusting()) return;
        String tag = ESListaTags.getSelectedValue();
        if(tag != null && tag.length() > 0){
            
            socket.emitGetTiposRoupaTag(tag, (message) -> {

                try {

                    assocTipoRoupas = GSON.fromJson(message.getMessage().getString("list"), new TypeToken<ArrayList<AssociacaoTagTipoRoupa>>() {}.getType());

                    ESListModelRoupas.clear();
                    ESListModelLeituras.clear();

                    ESFillListRoupas(assocTipoRoupas);

                } catch (JSONException ex) {
                    java.util.logging.Logger.getLogger(BitcliqLaundryRFIDProject.class.getName()).log(Level.SEVERE, null, ex);
                }

            });
        }
        
    }//GEN-LAST:event_ESListaTagsValueChanged

    private void ESListaTiposRoupaValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_ESListaTiposRoupaValueChanged
        if(evt.getValueIsAdjusting()) return;
        
        int index = ESListaTiposRoupa.getSelectedIndex();
        
        if(index < 0) return;
        if(ESListaTiposRoupa.getSelectedValue() != null && ESListaTiposRoupa.getSelectedValue().length() >= 0){

            AssociacaoTagTipoRoupa assocTipoRoupa = assocTipoRoupas.get(index);

            LeituraTagsDatas ltd = new LeituraTagsDatas();
            ltd.setTag(assocTipoRoupa.getTagID());
            ltd.setStart(assocTipoRoupa.getTimestamp());
            ltd.setEnd(assocTipoRoupa.getTimestampEnd());

            socket.emitGetLeiturasTagDatas(ltd, (message) -> {
                try {


                    ArrayList<Leitura> leituras = GSON.fromJson(message.getMessage().getString("list"), new TypeToken<ArrayList<Leitura>>() {}.getType());

                    ESFillListLeituras(leituras);

                } catch (JSONException ex) {
                    java.util.logging.Logger.getLogger(BitcliqLaundryRFIDProject.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
        
        
    }//GEN-LAST:event_ESListaTiposRoupaValueChanged

    private void CTStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CTStartActionPerformed
        Object[] options = { "Sim", "Cancelar" };
        int result = JOptionPane.showOptionDialog(null, "Irá limpar a tabela temporária das contagens (Primavera). Pretende mesmo fazer isto?", "Atenção", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        System.out.println(result);
        System.out.println(JOptionPane.YES_OPTION);
        if(result == JOptionPane.YES_OPTION)
            socket.emitCode(SocketCode.START_READINGS);
        
    }//GEN-LAST:event_CTStartActionPerformed

    private void CTStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CTStopActionPerformed
       
        socket.emitCode(SocketCode.END_READINGS);
        
    }//GEN-LAST:event_CTStopActionPerformed

    private void CFSaveTemporizadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CFSaveTemporizadorActionPerformed
        int temporizador = -1;
        try {
            temporizador = Integer.parseInt(CFTemporizador.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Indique um número para definir", "Erro", JOptionPane.ERROR_MESSAGE);
        }
        
        socket.emitSetTimerOff(temporizador);
    }//GEN-LAST:event_CFSaveTemporizadorActionPerformed

    private void CFGuardarPrimaveraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CFGuardarPrimaveraActionPerformed
        // TODO add your handling code here:
        isSavingDatabase = CFGuardarPrimavera.isSelected();
        updateToggleDatabase();
        socket.emitSetDatabaseStatus(isSavingDatabase, (message) -> {
            if(!message.isYes()){
                JOptionPane.showMessageDialog(null, "Ocorreu um erro ao trocar o estado da base de dados. Tente novamente.", "Erro", JOptionPane.ERROR_MESSAGE);
                isSavingDatabase = !isSavingDatabase;
                updateToggleDatabase();
            }
        });
        
    }//GEN-LAST:event_CFGuardarPrimaveraActionPerformed

    private void CFLimparBaseDeDadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CFLimparBaseDeDadosActionPerformed
        
        int result = JOptionPane.showConfirmDialog(null, "Tem a certeza que pretende limpar os dados da base de dados?", "Atenção", JOptionPane.WARNING_MESSAGE);
        if(result == JOptionPane.YES_OPTION){
            socket.emitCode(SocketCode.CLEAR_DATABASE, (message) -> {
                
                if(message.isYes()){
                    JOptionPane.showMessageDialog(null, "A base de dados foi limpa com sucesso", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Ocorreu um erro ao tentar limpar a base de dados. Tente novamente mais tarde.", "Erro", JOptionPane.ERROR_MESSAGE);
                }
                
            });
        }
    }//GEN-LAST:event_CFLimparBaseDeDadosActionPerformed

    private void ESAtualizarTagsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ESAtualizarTagsActionPerformed
        
        ESListModelLeituras.clear();
        ESListModelRoupas.clear();
        ESListModelTags.clear();
        
        tags.clear();
        socket.emitCode(SocketCode.GET_TAGS_READ, (message) -> {
            
            try {
                
                tags = GSON.fromJson(message.getMessage().getString("list"), new TypeToken<ArrayList<String>>() {}.getType());
                
                ESFillListTags(tags);
                
                
            } catch (JSONException ex) {
                java.util.logging.Logger.getLogger(BitcliqLaundryRFIDProject.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        });
        
        
        
    }//GEN-LAST:event_ESAtualizarTagsActionPerformed

    private void guiTabsPannelStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_guiTabsPannelStateChanged
        int index = guiTabsPannel.getSelectedIndex();
        if(index != 0){
            long now = System.currentTimeMillis() / 1000L;
            
            if(now <= passwordExpires) {
                return;
            }
            
            JPasswordField pf = new JPasswordField(20);
            JLabel mensagem = new JLabel("Indique a password:");
            JPanel panelPassword = new JPanel();
            panelPassword.add(mensagem);
            panelPassword.add(pf);
            JOptionPane.showConfirmDialog(null, panelPassword, "Indique a password para abrir o separador", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

            String password = new String(pf.getPassword());
            if(password.equals("")){
                guiTabsPannel.setSelectedIndex(0);
                return;
            }
            
            if(!GeneralUtils.md5(password).equals(PASSWORD)){
                JOptionPane.showMessageDialog(null, "A palavra-passe está errada", "Password errada", JOptionPane.ERROR_MESSAGE);
                guiTabsPannel.setSelectedIndex(0);
            } else {
                passwordExpires = (System.currentTimeMillis() / 1000L) + minutesToExpire * 60;
                
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        guiTabsPannel.setSelectedIndex(0);
                    }
                }, minutesToExpire * 60 * 1000);
            }
            
        }
        
    }//GEN-LAST:event_guiTabsPannelStateChanged

    private void CTContinuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CTContinuarActionPerformed
        socket.emitCode(SocketCode.CONTINUE_READINGS);
    }//GEN-LAST:event_CTContinuarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            LOG.fatal("EXCEPTION", ex);
        }
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BitcliqLaundryRFIDProject().setVisible(true);
            }
        });
    }

    public int mockupReadingType() {
        switch (MKDefinicoesComboBox.getSelectedIndex()) { // define quais são as tags para receber
            case 0:
                return MockupReadingTypes.MK_TAGS_REGISTADAS_TOTALIDADE;
            case 1:
                return MockupReadingTypes.MK_TAGS_REGISTADAS_FORNECEDOR;
            case 2:
                return MockupReadingTypes.MK_TAGS_NAO_REGISTADAS;
        }
        return -1;
    }
    
    private void initialStartup(){
        socket.emitCode(SocketCode.GET_STARTUP_SETTINGS, (SocketMessage message) -> {
            try {
                roupas = GSON.fromJson(message.getMessage().getString("lista_roupas"), new TypeToken<ArrayList<TipoRoupa>>() {}.getType());
                Collections.sort(roupas, Comparator.comparing(TipoRoupa::getNome));
                
                int timerOff = message.getMessage().getInt("timerOff");
                CFTemporizador.setText(timerOff + "");
                isSavingDatabase = message.getMessage().getBoolean("database");
                updateToggleDatabase();
                for (TipoRoupa roupa : roupas) { // adiciona o tipo de roupas à combo box para definir o tipo de roupa
                    SRComboBoxRoupa.addItem(roupa.getNome());
                }
                
            } catch (JSONException ex) {
                LOG.fatal("JSONException loadTiposRoupa()", ex);
            }

        });
    }
    
    private void updateToggleDatabase(){
        CFGuardarPrimavera.setSelected(isSavingDatabase);
        CFGuardarPrimavera.setText(isSavingDatabase ? "ON" : "OFF");
    }
    
    private void ESFillListTags(ArrayList<String> tags) {
        ESListModelTags.clear();
        if(tags != null && tags.size() > 0) {
            ESListaTags.setEnabled(true);
            for(String tag : tags) {
                ESListModelTags.addElement(tag);
            }
        } else {
            ESListaTags.setEnabled(false);
            ESListModelTags.addElement("Sem dados");
        }
        ESFillListRoupas(null);
        
    }
    
    private void ESFillListRoupas(ArrayList<AssociacaoTagTipoRoupa> roupas) {
        ESListModelRoupas.clear();
        if(roupas != null && roupas.size() > 0) {
            ESListaTiposRoupa.setEnabled(true);
            for(AssociacaoTagTipoRoupa roupa : roupas) {
                ESListModelRoupas.addElement(roupa.getTipoRoupa().getNome() + " ("+ GeneralUtils.formatedDate(roupa.getTimestamp()) + " - " + GeneralUtils.formatedDate(roupa.getTimestampEnd()) + ")");
            }
        } else {
            ESListaTiposRoupa.setEnabled(false);
            ESListModelRoupas.addElement("Sem dados");
        }
        
        ESFillListLeituras(null);
    }
    
    private void ESFillListLeituras(ArrayList<Leitura> leituras) {
        ESListModelLeituras.clear();
        if(leituras != null && leituras.size() > 0) {
            ESListaLeituras.setEnabled(true);
            for(Leitura leitura : leituras) {
                ESListModelLeituras.addElement(GeneralUtils.formatedDate(leitura.getTimestamp()) + " " + (leitura.isEntrada() ? "Entrada" : "Saida"));
            }
        } else {
            ESListaLeituras.setEnabled(false);
            ESListModelLeituras.addElement("Sem dados");
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton CFGuardarPrimavera;
    private javax.swing.JButton CFLimparBaseDeDados;
    private javax.swing.JButton CFSaveTemporizador;
    private javax.swing.JTextField CFTemporizador;
    private javax.swing.JButton CTContinuar;
    private javax.swing.JTextArea CTListaContagem;
    private javax.swing.JProgressBar CTProgressBar;
    private javax.swing.JButton CTStart;
    private javax.swing.JButton CTStop;
    private javax.swing.JLabel CTTagsContadas;
    private javax.swing.JLabel CTTagsContadasComRoupa;
    private javax.swing.JLabel CTTagsContadasSemRoupa;
    private javax.swing.JPanel ConfiguracoesPanel;
    private javax.swing.JPanel ContagemPanel;
    private javax.swing.JButton ESAtualizarTags;
    private javax.swing.JList<String> ESListaLeituras;
    private javax.swing.JList<String> ESListaTags;
    private javax.swing.JList<String> ESListaTiposRoupa;
    private javax.swing.JPanel EstatisticasPanel;
    private javax.swing.JComboBox<String> MKDefinicoesComboBox;
    private javax.swing.JButton MKIniciarButton;
    private javax.swing.JButton MKPararButton;
    private javax.swing.JButton MKReiniciarLeitura;
    private javax.swing.JToggleButton MKToggleButton;
    private javax.swing.JPanel MockupPanel;
    private javax.swing.JComboBox<String> SRComboBoxRoupa;
    private javax.swing.JButton SRSetupButton;
    private javax.swing.JLabel SRTagsContadas;
    private javax.swing.JTextField STNomeFornecedor;
    private javax.swing.JTextField STRefEncomenda;
    private javax.swing.JButton STSetupButton;
    private javax.swing.JLabel STTagsContadas;
    private javax.swing.JPanel SetupTagsFornecedorPanel;
    private javax.swing.JPanel SetupTiposRoupaPanel;
    private javax.swing.JTabbedPane guiTabsPannel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables

}
