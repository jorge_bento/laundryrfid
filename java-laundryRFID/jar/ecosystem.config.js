module.exports = {
	apps: [{
		name: 'socketServer',
		script: 'index.js',
		cwd: 'LaundryRFID-SocketServer',
	}, {
		name: 'mainService',
		args: [
			'-jar',
			'LaundryRFID-MainService.jar'
		],
		script: 'java',
	}]
}