var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = 8888;

io.on('connection', function(socket){
    socket.on('message', function(msg){
        console.log("new message: "+msg);
        socket.broadcast.emit('message', msg);
    });

    socket.on('reply', function(msg){
        console.log("new reply: "+msg);
        socket.broadcast.emit('reply', msg);
    });
});

http.listen(port, function(){
    console.log('listening on 127.0.0.1:' + port);
});
