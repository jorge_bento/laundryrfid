/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.models;

import com.bitcliq.laundryrfid.common.util.SocketConnection;

/**
 *
 * @author andre
 */
public interface Command {
    
    interface ExecutionOver {
        void over();
    }
    
    String command();
    String help();
    String usage();
    String example();
    void run(String[] args, SocketConnection socket, ExecutionOver over);
    
}
