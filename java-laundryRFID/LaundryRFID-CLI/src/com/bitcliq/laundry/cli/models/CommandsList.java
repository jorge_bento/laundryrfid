/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.models;

import com.bitcliq.laundry.cli.commands.Contagem;
import com.bitcliq.laundry.cli.commands.Continue;
import com.bitcliq.laundry.cli.commands.Count;
import com.bitcliq.laundry.cli.commands.Ping;
import com.bitcliq.laundry.cli.commands.Reading;
import com.bitcliq.laundry.cli.commands.RegisterProvider;
import com.bitcliq.laundry.cli.commands.RegisterTagsClothing;
import com.bitcliq.laundry.cli.commands.Start;
import com.bitcliq.laundry.cli.commands.Status;
import com.bitcliq.laundry.cli.commands.Stop;
import com.bitcliq.laundry.cli.commands.TipoRoupa;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author andre
 */
public class CommandsList {
    private HashMap<String, Command> commands = null;
    
    public void loadCommands(){
        
        if(commands == null){
            commands = new HashMap<>();
        
            commands.put(Count.getCommandName(), new Count());
            commands.put(Ping.getCommandName(), new Ping());
            commands.put(Reading.getCommandName(), new Reading());
            commands.put(Start.getCommandName(), new Start());
            commands.put(Continue.getCommandName(), new Continue());
            commands.put(Stop.getCommandName(), new Stop());
            commands.put(TipoRoupa.getCommandName(), new TipoRoupa());
            commands.put(RegisterTagsClothing.getCommandName(), new RegisterTagsClothing());
            commands.put(RegisterProvider.getCommandName(), new RegisterProvider());
            commands.put(Status.getCommandName(), new Status());
            commands.put(Contagem.getCommandName(), new Contagem());
            
            
        }
        
    }
    
    public boolean commandExists(String command){
        return commands.containsKey(command);
    }
    
    public Command getCommand(String command){
        if(commandExists(command)){
            return commands.get(command);
        } else {
            return null;
        }
    }
    
    public int length(){
        return commands.size();
    }
    
    
    public String getHelpList(){
        String message = " =-=-= LISTA DE COMANDOS =-=-=\n\n";
        
        for (Map.Entry<String, Command> entry : commands.entrySet()) {
            message += "  > "+ entry.getValue().command() + "\n    " + entry.getValue().help() + "\n\n";
        }
        
        message += "   help <comando> para mais detalhes\n";
        
        return message;
    }
    
    public String getHelpCommand(String cmd){
        String message = " =-=-= AJUDA DO COMANDO "+cmd.toUpperCase()+" =-=-=\n\n";
        Command command = commands.get(cmd);
        
        message += "   Detalhes: \n";
        message += "        " + command.help() + "\n\n";
        message += "   Sintaxe: \n";
        message += "        " + command.usage() + "\n\n";
        message += "   Exemplo: \n";
        message += "        " + command.example() + "\n";
        
        return message;
    }
    
    
    public static void sendError(Command.ExecutionOver callback, String message){
        if(callback == null || message == null) return;
        
        try {
            JSONObject reply = new JSONObject();
            reply.put("status", "error");
            reply.put("error", message);
            System.out.println(reply.toString());
            callback.over();
        } catch (JSONException ex) {
            System.out.println(message);
            Logger.getLogger(Reading.class.getName()).log(Level.SEVERE, null, ex);
            callback.over();
        }
    }
    
}
