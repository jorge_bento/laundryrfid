/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli;

import com.bitcliq.laundry.cli.models.CommandsList;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import java.util.Arrays;

/**
 *
 * @author andre
 */
public class Cli {

    /**
     * @param a the command line arguments
     */
    public static void main(String[] a) {

        final CommandsList commands = new CommandsList();
        final Arguments args = new Arguments();
        args.setArgs(a);

        commands.loadCommands();

        //args.setArgs(new String[]{"contagem", "2"});
        if (args.getArgs().length == 0) {

            System.out.println(commands.getHelpList());

            return;
        }

        if (args.getArgs(0).equalsIgnoreCase("help")) {

            if (args.getArgs(1) != null && commands.commandExists(args.getArgs(1).toLowerCase())) {

                System.out.println(commands.getHelpCommand(args.getArgs(1)));

            } else {

                System.out.println(commands.getHelpList());

            }

        } else if (commands.commandExists(args.getArgs(0).toLowerCase())) {

                
            SocketConnection socket = new SocketConnection();
            
            socket.onceConnect(() -> {

                String[] arguments = Arrays.copyOfRange(args.getArgs(), 1, args.length());

                commands.getCommand(args.getArgs(0)).run(arguments, socket, () -> {
                    socket.disconnect();
                    System.exit(0);
                });
            });
            
            socket.connect();

        } else {
            System.out.println("That command doesn't exist. Run help for the list of commands.");
        }

    }

}

class Arguments {

    private String[] args;

    public String[] getArgs() {
        return args;
    }

    public String getArgs(int index) {
        if (index <= args.length - 1) {
            return args[index];
        }
        return null;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public int length() {
        return args.length;
    }

}
