/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.commands;

import com.bitcliq.laundry.cli.models.Command;
import com.bitcliq.laundry.cli.models.CommandsList;
import com.bitcliq.laundryrfid.common.models.SocketCode;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author andre
 */
public class Reading implements Command {

    private static String command = "reading";

    public static String getCommandName() {
        return command;
    }

    @Override
    public String command() {
        return command;
    }

    @Override
    public String help() {
        return "Faz a gestão das leituras";
    }

    @Override
    public String usage() {
        return command + " <get|set> [mock|hardware]";
    }

    @Override
    public String example() {
        return command + " set hardware";
    }

    @Override
    public void run(String[] args, SocketConnection socket, Command.ExecutionOver callback) {

        JSONObject reply = new JSONObject();

        if (args.length != 0) {

            if (args[0].toLowerCase().equals("get")) {

                socket.emitCode(SocketCode.IS_MOCK_READING, (message) -> {
                    try {
                        if (message != null) {

                            reply.put("status", "success");
                            reply.put("reading", message.isYes() ? "mockup" : "hardware");

                            System.out.println(reply.toString());
                            callback.over();

                        } else {
                            CommandsList.sendError(callback, "Erro na sintaxe. Comando não reconhecido");
                        }

                    } catch (JSONException ex) {
                        System.err.println(message.getMessage());
                        Logger.getLogger(Reading.class.getName()).log(Level.SEVERE, null, ex);
                        callback.over();
                    }

                });

            } else if (args[0].toLowerCase().equals("set")) {

                if (args.length >= 2) {

                    if (args[1].equalsIgnoreCase("mock") || args[1].equalsIgnoreCase("hardware")) {

                        boolean isMock = args[1].equalsIgnoreCase("mock");

                        socket.emitChangeReadingType(isMock, (message) -> {

                            if (message == null)  CommandsList.sendError(callback, "Ocorreu um erro.");
                            else 

                            socket.emitClearReadTags((message1) -> {
                                
                                if (message1 == null)  CommandsList.sendError(callback, "Ocorreu um erro.");
                                else 

                                socket.emitCode(SocketCode.START_READINGS, (message2) -> {
                                    if (message2 == null)  CommandsList.sendError(callback, "Ocorreu um erro.");
                                    else 
                                    
                                    try {

                                        reply.put("status", "success");

                                        System.out.println(reply.toString());
                                        callback.over();

                                    } catch (JSONException ex) {
                                        System.err.println(message.getMessage());
                                        Logger.getLogger(Reading.class.getName()).log(Level.SEVERE, null, ex);
                                        callback.over();
                                    }

                                });

                            });

                        });
                    } else {
                        CommandsList.sendError(callback, "Erro na sintaxe. O segundo campo tem de ser mock ou hardware");
                    }

                } else {
                    CommandsList.sendError(callback, "Erro na sintaxe. Verifique a utilização.");
                }

            } else {
                CommandsList.sendError(callback, "Erro na sintaxe. Comando não reconhecido");
            }
        } else {
            CommandsList.sendError(callback, "Erro na sintaxe. Verifique a utilização.");
        }
    }
}
