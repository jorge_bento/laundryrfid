/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.commands;

import com.bitcliq.laundry.cli.models.Command;
import com.bitcliq.laundry.cli.models.CommandsList;
import com.bitcliq.laundryrfid.common.models.Lote;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author estagiario
 */
public class RegisterProvider implements Command {
    
    private static String command = "register-provider";

    public static String getCommandName() {
        return command;
    }

    @Override
    public String command() {
        return command;
    }

    @Override
    public String help() {
        return "Regista o Fornecedor das tags lidas";
    }

    @Override
    public String usage() {
        return command + " <nome Fornecedor> ,, <refEncomenda>";
    }

    @Override
    public String example() {
        return command + " Nome do Fornecedor 1 ,, AGH4151975";
    }

    @Override
    public void run(String[] args, SocketConnection socket, Command.ExecutionOver callback) {

        
        
        if (args != null && args.length >= 1) {
            
            args = String.join(" ", args).split(",,");
            for(int i = 0; i < args.length; i++){
                args[i] = args[i].trim();
            }
            if(args.length != 2){
                CommandsList.sendError(callback, "Verifique a sintaxe");
                return;
            }
            
            Lote lote = new Lote(args[0], args[1]);
            
            socket.emitRegisterProviderInformation(lote, (message) -> {
                
                if (message.isYes()) {
                    try {
                        JSONObject reply = new JSONObject();
                        reply.put("status", "success");
                        System.out.println(reply.toString());
                        callback.over();
                    } catch (JSONException ex) {
                        System.out.println(message.getMessage());
                        Logger.getLogger(RegisterProvider.class.getName()).log(Level.SEVERE, null, ex);
                        callback.over();
                    }
                } else {
                    String errorMsg = "Ocorreu um erro ao registar as tags.";
                    if (message.getMessage().has("error")) {
                        try {
                            errorMsg = message.getMessage().getString("error");
                        } catch (JSONException ex) {
                            Logger.getLogger(RegisterProvider.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    CommandsList.sendError(callback, errorMsg);
                }
            });
            
        } else {
            CommandsList.sendError(callback, "Sintaxe errada. Verifique a ajuda");
        }
    }
    
}
