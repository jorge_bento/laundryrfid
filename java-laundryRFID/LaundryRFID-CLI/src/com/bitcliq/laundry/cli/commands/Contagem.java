/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.commands;

import com.bitcliq.laundry.cli.models.Command;
import com.bitcliq.laundry.cli.models.CommandsList;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author estagiario
 */
public class Contagem implements Command {
    
    private static String command = "contagem";
    
    public static String getCommandName(){
        return command;
    }
    
    @Override
    public String command() {
        return command;
    }

    @Override
    public String help() {
        return "Obtém a última contagem não processada";
    }

    @Override
    public String usage() {
        return command + " ";
    }

    @Override
    public String example() {
        return command + " ";
    }

    @Override
    public void run(String[] args, SocketConnection socket, Command.ExecutionOver callback) {
        
        int limit = -1;
        if(args != null && args.length >= 1){
            try {
                limit = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                CommandsList.sendError(callback, "Têm de definir um limite, ou remover o limite para usar 20 (por defeito)");
                limit = -1;
            }
        }
        
        
        socket.emitGetCountingNotProccessed(limit, (message) -> {
            try {
                JSONObject reply = new JSONObject();
                if(message != null && message.getMessage() != null){
                    reply.put("status", "success");
                    reply.put("list", message.getMessage().getString("list"));
                } else {
                    reply.put("status", "error");
                    reply.put("error", "Não foi possível obter o ping do serviço.");
                }
                System.out.println(reply.toString());
                callback.over();
            } catch (JSONException ex) {
                Logger.getLogger(Count.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
    }
    
}
