/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.commands;

import com.bitcliq.laundry.cli.models.Command;
import com.bitcliq.laundryrfid.common.models.SocketCode;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author andre
 */
public class Ping implements Command {
    private static String command = "ping";
    
    public static String getCommandName(){
        return command;
    }
    
    @Override
    public String command() {
        return command;
    }

    @Override
    public String help() {
        return "Verifica se o serviço está online";
    }

    @Override
    public String usage() {
        return command + " ";
    }

    @Override
    public String example() {
        return command + " ";
    }

    @Override
    public void run(String[] args, SocketConnection socket, Command.ExecutionOver callback) {
        long start = System.nanoTime();
        socket.emitCode(SocketCode.PING, (message) -> {
            try {
                JSONObject reply = new JSONObject();
                if(message != null && message.getMessage() != null){
                    float taken = (System.nanoTime() - start) / 1000000;
                    reply.put("status", "success");
                    reply.put("timeTakenMs", taken);
                } else {
                    reply.put("status", "error");
                    reply.put("error", "Não foi possível obter o ping do serviço.");
                }
                System.out.println(reply.toString());
                callback.over();
            } catch (JSONException ex) {
                Logger.getLogger(Count.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
    }
    
}
