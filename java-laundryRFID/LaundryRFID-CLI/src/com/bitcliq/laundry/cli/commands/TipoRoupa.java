/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.commands;

import com.bitcliq.laundry.cli.models.Command;
import com.bitcliq.laundryrfid.common.models.SocketCode;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author estagiario
 */
public class TipoRoupa implements Command {
    
    private static String command = "tipo-roupa";
    
    public static String getCommandName(){
        return command;
    }
    
    @Override
    public String command() {
        return command;
    }

    @Override
    public String help() {
        return "Verifica os tipos de Roupa existentes";
    }

    @Override
    public String usage() {
        return command + "";
    }

    @Override
    public String example() {
        return command + " ";
    }

    @Override
    public void run(String[] args, SocketConnection socket, Command.ExecutionOver callback) {
        socket.emitCode(SocketCode.GET_TYPES_CLOTHING, (message) -> {
            try {
                JSONObject reply = new JSONObject();
                if(message != null && message.isYes()){
                    reply.put("status", "success");
                    reply.put("list", message.getMessage().getString("list"));
                } else {
                    reply.put("status", "error");
                    reply.put("error", "Ocorreu um erro ao obter a lista de roupas");
                }
                System.out.println(reply.toString());
                callback.over();
            } catch (JSONException ex) {
                System.out.println(message.getMessage());
                Logger.getLogger(Count.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
    }
    
}
