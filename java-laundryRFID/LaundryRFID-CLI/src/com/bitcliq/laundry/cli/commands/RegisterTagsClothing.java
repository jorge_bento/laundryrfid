/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundry.cli.commands;

import com.bitcliq.laundry.cli.models.Command;
import com.bitcliq.laundry.cli.models.CommandsList;
import com.bitcliq.laundryrfid.common.models.TipoRoupa;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author estagiario
 */
public class RegisterTagsClothing implements Command {

    private static String command = "register-tags-clothing";

    public static String getCommandName() {
        return command;
    }

    @Override
    public String command() {
        return command;
    }

    @Override
    public String help() {
        return "Regista as tags que foram lidas com um novo tipo de roupa";
    }

    @Override
    public String usage() {
        return command + " <tipoRoupaID>";
    }

    @Override
    public String example() {
        return command + " 2";
    }

    @Override
    public void run(String[] args, SocketConnection socket, Command.ExecutionOver callback) {

        if (args != null && args.length == 1 && args[0].length() >= 1) {
            int id = -1;
            try {
                id = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                CommandsList.sendError(callback, "O ID têm de ser um número");
            }

            TipoRoupa roupa = new TipoRoupa();
            roupa.setTipoRoupaID(id);
            socket.emitRegisterClothingInformation(roupa, (message) -> {
                if (message.isYes()) {
                    try {
                        JSONObject reply = new JSONObject();
                        reply.put("status", "success");
                        System.out.println(reply.toString());
                        callback.over();
                    } catch (JSONException ex) {
                        System.out.println(message.getMessage());
                        Logger.getLogger(RegisterTagsClothing.class.getName()).log(Level.SEVERE, null, ex);
                        callback.over();
                    }
                } else {
                    String errorMsg = "Ocorreu um erro ao registar as tags. (Pode ter faltado registar o fornecedor)";
                    if (message.getMessage().has("error")) {
                        try {
                            errorMsg = message.getMessage().getString("error");
                        } catch (JSONException ex) {
                            Logger.getLogger(RegisterTagsClothing.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    CommandsList.sendError(callback, errorMsg);
                }
            });
        } else {
            CommandsList.sendError(callback, "Indique o ID do tipo de roupa");
        }
    }

}
