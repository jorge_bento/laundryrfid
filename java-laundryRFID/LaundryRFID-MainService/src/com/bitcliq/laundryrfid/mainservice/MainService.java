/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.mainservice;

import com.bitcliq.laundryrfid.common.listener.TagListener;
import com.bitcliq.laundryrfid.common.models.AssociacaoTagTipoRoupa;
import com.bitcliq.laundryrfid.common.models.Ciclo;
import com.bitcliq.laundryrfid.common.models.ContagemClassificacao;
import com.bitcliq.laundryrfid.common.models.Leitura;
import com.bitcliq.laundryrfid.common.models.LeituraTagsDatas;
import com.bitcliq.laundryrfid.common.models.Lote;
import com.bitcliq.laundryrfid.common.models.SocketCode;
import com.bitcliq.laundryrfid.common.models.SocketMessage;
import com.bitcliq.laundryrfid.common.models.Tag;
import com.bitcliq.laundryrfid.common.models.TipoRoupa;
import com.bitcliq.laundryrfid.common.util.DatabaseRepo;
import com.bitcliq.laundryrfid.common.util.GeneralUtils;
import com.bitcliq.laundryrfid.common.util.Singleton;
import com.bitcliq.laundryrfid.common.util.SocketConnection;
import com.bitcliq.laundryrfid.mainservice.factory.rfidreader.RFIDFactory;
import com.bitcliq.laundryrfid.mainservice.factory.rfidreader.RFIDReader;
import com.google.gson.Gson;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Timer;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;

/**
 *
 * @author andre
 */
public class MainService {

    private static final Singleton singleton = Singleton.getInstance();
    private static HashMap<String, Tag> readTags = new HashMap<String, Tag>();
    private static HashMap<Integer, ContagemClassificacao> contagem = new HashMap<>();
    private static final DatabaseRepo dbRepo = new DatabaseRepo();
    private static int timerCount = 0;
    private static int timerOff = 0;
    private static SocketConnection socket = null;
    private static final Timer timer = new Timer(1, null);
    private static final Logger LOG = GeneralUtils.getLogger();
    private static final HashMap<String, Tag> bufferTagsReading = new HashMap<>();
    private static boolean savingToDatabase = true;
    private static final Gson GSON = new Gson();
    private static RFIDReader reader = null; // Factory para o reader

    public static void main(String[] args) {
        LOG.info("Bitcliq - LaundryRFID Project v"+singleton.getVersion());
        LOG.info("A iniciar o serviço.");
        
        timer.removeActionListener(null);

        socket = new SocketConnection();

        timerOff = singleton.getConfig().getTimer();

        // Quando houver conexão com o socket
        socket.onConnect(() -> {
            LOG.info("Conectado ao Socket");

            socket.on("message", (SocketMessage message) -> {
                boolean autoReplyYes = false;
                try {
                    LOG.debug(message.getMessage());
                    switch (message.getCode()) {
                        case SocketCode.PING:
                            
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.END_READINGS:
                            
                            stopReadings(); // para a leitura
                            timer.stop();
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.START_READINGS:
                            
                            dbRepo.cleanContagemClassificacao();
                            startReadings(); // inicia a leitura
                            clearReadTags();
                            timerCount = 0;
                            timer.start();
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.CONTINUE_READINGS:
                            
                            startReadings(); // inicia a leitura
                            clearReadTags();
                            timerCount = 0;
                            timer.start();
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.IS_MOCK_READING:
                            
                            if (message.needsReply()) { // devolve YES ou NO caso precisar de resposta
                                socket.reply(singleton.isMockRFIDreader() ? SocketCode.REPLY_YES : SocketCode.REPLY_NO, message);
                            }   
                            
                            break;
                            
                        case SocketCode.SET_MOCK_READING_TYPE:
                            
                            singleton.setMockupReadingType(message.getMessage().getInt("type"));
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.SET_MOCK_READING:
                            
                            boolean isMock = message.getMessage().getBoolean("isMock");
                            singleton.setMockRFIDreader(isMock);
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.CLEAR_READ_TAGS:
                            
                            clearReadTags();
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.GET_TYPES_CLOTHING:
                            
                            message.editMessage("list", GSON.toJson(dbRepo.getTiposDeRoupa()));
                            socket.reply(SocketCode.REPLY_YES, message);
                            
                            break;
                            
                        case SocketCode.REGISTER_TAGS_PROVIDER:
                            
                            Lote lote = GSON.fromJson(message.getMessage().getString("lote"), Lote.class);
                            Lote newLote = dbRepo.insertLote(lote);
                            if (newLote != null) {
                                if (dbRepo.insertTags(readTags, lote)) {
                                    socket.reply(SocketCode.REPLY_YES, message);
                                } else {
                                    message.editMessage("error", "Ocorreu um erro ao registar as tags.");
                                    socket.reply(SocketCode.REPLY_NO, message);
                                }
                            } else {
                                message.editMessage("error", "Ocorreu um erro ao criar o fornecedor.");
                                socket.reply(SocketCode.REPLY_NO, message);
                            }   
                            
                            break;
                            
                        case SocketCode.REGISTER_TAGS_CLOTHING:
                            
                            // buscar o tipo de roupa selecionado
                            TipoRoupa roupa = GSON.fromJson(message.getMessage().getString("tipoRoupa"), TipoRoupa.class);
                            if (dbRepo.isTipoRoupaRegisted(roupa)) {
                                // inserir a associação na BD
                                if (dbRepo.insertAssociacaoTagRoupa(readTags, roupa)) {
                                    socket.reply(SocketCode.REPLY_YES, message);
                                } else {
                                    socket.reply(SocketCode.REPLY_NO, message);
                                }
                            } else {
                                message.editMessage("error", "Não existe a tag com esse ID");
                                socket.reply(SocketCode.REPLY_NO, message);
                            }   
                            
                            break;
                            
                        case SocketCode.GET_COUNT_READ_TAGS:
                            
                            message.editMessage("count", readTags.size());
                            socket.reply(SocketCode.REPLY_YES, message);
                            
                            break;
                            
                        case SocketCode.GET_STATUS:
                            
                            message.editMessage("isRunning", isRunning());
                            message.editMessage("isMockup", singleton.isMockRFIDreader());
                            message.editMessage("mockReadingType", singleton.getMockupReadingType());
                            socket.reply(SocketCode.REPLY_YES, message);
                            
                            break;
                            
                        case SocketCode.GET_CONTAGEM_CLASSIFICACAO:
                            
                            int limit = message.getMessage().has("limit") ? message.getMessage().getInt("limit") : -1;
                            message.editMessage("list", GSON.toJson(dbRepo.getContagensNaoProcessadas(limit)));
                            socket.reply(SocketCode.REPLY_YES, message);
                           
                            break;
                            
                        case SocketCode.GET_STARTUP_SETTINGS:
                            
                            message.editMessage("database", savingToDatabase);
                            message.editMessage("timerOff", timerOff);
                            message.editMessage("lista_roupas", GSON.toJson(dbRepo.getTiposDeRoupa()));
                            socket.reply(SocketCode.REPLY_YES, message);
                            
                            break;
                            
                        case SocketCode.SET_DATABASE_STATUS:
                            
                            savingToDatabase = message.getMessage().getBoolean("status");
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.SET_TIMER_OFF: 
                            
                            timerOff = message.getMessage().getInt("timer");
                            autoReplyYes = true;
                            
                            break;
                            
                        case SocketCode.CLEAR_DATABASE:
                            
                            boolean result = dbRepo.clearDatabase();
                            
                            if(message.needsReply())
                                socket.reply(result ? SocketCode.REPLY_YES : SocketCode.REPLY_NO , message);
                            
                            break;
                            
                        case SocketCode.GET_TAGS_READ: 
                            
                            message.editMessage("list", GSON.toJson(bufferTagsReading.keySet()));
                            socket.reply(SocketCode.REPLY_YES, message);
                            
                            break;
                            
                        case SocketCode.GET_TIPOS_ROUPA_TAG:
                            
                            if(!message.getMessage().has("tag")){
                                socket.reply(SocketCode.REPLY_NO, message);
                                
                            } else {
                                ArrayList<AssociacaoTagTipoRoupa> assocTipoRoupas = dbRepo.getTiposRoupaTag(message.getMessage().getString("tag"));
                                message.editMessage("list", GSON.toJson(assocTipoRoupas));
                                socket.reply(SocketCode.REPLY_YES, message);
                            }
                            
                            break;
                            
                        case SocketCode.GET_LEITURAS_TAGS_DATAS:
                            
                            if(!message.getMessage().has("leitura")){
                                socket.reply(SocketCode.REPLY_NO, message);
                                
                            } else {
                                LeituraTagsDatas leitura;
                                leitura = GSON.fromJson(message.getMessage().getString("leitura"), LeituraTagsDatas.class);


                                ArrayList<Leitura> leituras = dbRepo.getLeiturasTagDatas(leitura.getTag(), leitura.getStart(), leitura.getEnd());
                                message.editMessage("list", GSON.toJson(leituras));
                                socket.reply(SocketCode.REPLY_YES, message);
                            }
                            
                            break;
                            
                            
                        default:
                            
                            break;
                    }
                    
                    

                    if (autoReplyYes) {
                        if (message.needsReply()) {
                            socket.reply(SocketCode.REPLY_YES, message);
                        }
                    }

                } catch (JSONException ex) {
                    LOG.fatal("JSONException na mensagem: "+message.getMessage().toString(), ex);
                }
            });

            startReadings(); // inicia as leituras (dependente do tipo de leitura mock, hardware)

            ActionListener taskPerformer = (ActionEvent evt) -> {
                runTimer();
            };
            
            timer.addActionListener(taskPerformer);
            timer.setDelay(1000);
            timer.start();

        });
        
        socket.onConnectionFailed(() -> {
            
            LOG.error("Não foi possível conectar ao servidor de comunicação entre aplicações (socket). Esta aplicação irá funcionar como esperado, mas a Interface Gráfica e a API por linha de comandos não irão funcionar");
            
            startReadings(); // inicia as leituras (dependente do tipo de leitura mock, hardware)

            ActionListener taskPerformer = (ActionEvent evt) -> {
                runTimer();
            };
            
            timer.addActionListener(taskPerformer);
            timer.setDelay(1000);
            timer.start();
            
        });
        
        socket.connect();

        singleton.setTagListener(new TagListener() {
            @Override
            public void foundTag(Tag tag) { // Listener para quando uma tag é encontrada
                long start = System.nanoTime();
                if(tag.getTagID().length() <= 2){
                    LOG.info("IGNORED TAG: " + tag.getTagID());
                    return;
                }
                if(!tag.getTagID().startsWith("E2")){
                    LOG.info("IGNORED TAG: " + tag.getTagID());
                    return;
                }
                
                if(!bufferTagsReading.containsKey(tag.getTagID())){
                    bufferTagsReading.put(tag.getTagID(), tag);
                    
                

                    timerCount = 0;
                    LOG.info("Found new tag: " + tag.getTagID() + " "+ bufferTagsReading.size());
                    readTags.put(tag.getTagID(), tag);
                    tag = dbRepo.getTag(tag);

                    if (savingToDatabase && dbRepo.isTagRegistered(tag.getTagID())) {
                        Leitura leitura = new Leitura();
                        leitura.setTimestamp();
                        leitura.setTagID(tag.getTagID());
                        leitura.setEntradaSaidaInt(dbRepo.tagEntradaOuSaida(leitura));
                        leitura = dbRepo.insertLeitura(leitura);
                    }
                    
                    if (tag.getUltimoTipoRoupa() != null) { // se a tag tiver um tipo de roupa
                        if (!contagem.containsKey(tag.getUltimoTipoRoupa().getTipoRoupaID())) { // se esse tipo de roupa não estiver na lista de contagens
                            ContagemClassificacao classificacao = new ContagemClassificacao();
                            classificacao.setContador(1); // inicia o contador a 1
                            classificacao.setTipoRoupaID(tag.getUltimoTipoRoupa().getTipoRoupaID());
                            classificacao.setTipoRoupa(tag.getUltimoTipoRoupa());
                            classificacao.setProcessado(false);
                            contagem.put(classificacao.getTipoRoupaID(), classificacao); // adiciona à lista
                        } else {
                            contagem.get(tag.getUltimoTipoRoupa().getTipoRoupaID()).incrementCounter(); // se ja estiver na lista incrementa o contador
                        }
                        socket.emitPiecesCounterChanged(contagem); // notifica que a lista de contagens foi atualizada
                    }
                    
                    int comRoupa = getCountClassificacao();
                    int semRoupa = readTags.size() - comRoupa;
                    socket.emitTotalCounterChanged(comRoupa, semRoupa); // notifica que o contador de peças alterou
                
                }
                
                /*
                long end = System.nanoTime();
                long taken = (end-start)/1000000;
                if(taken != 0)
                    LOG.info("Time taken: " + (end-start)/1000000);*/
            }
        });

    }

    // obter a lista das tags que foram lidas
    public static HashMap<String, Tag> getTags() {
        return readTags;
    }

    public static void clearReadTags() {
        readTags.clear(); // limpa as tags lidas
        contagem.clear(); // limpa as tags contadas
        bufferTagsReading.clear();
        int comRoupa = getCountClassificacao();
        int semRoupa = readTags.size() - comRoupa;
        socket.emitTotalCounterChanged(comRoupa, semRoupa); // notifica que o contador de peças alterou
        socket.emitPiecesCounterChanged(contagem); // notifica que a contagem de tipos de roupa alterou
    }
    
    public static void runTimer(){
        timerCount++;
        socket.emitTimerChanged(timerCount, timerOff);
        if (timerCount >= timerOff) {
            timerCount = 0;
            if (contagem.size() > 0 && readTags.size() > 0) {
                
                if(savingToDatabase){
                    
                    Ciclo ciclo = dbRepo.createCycle();
                    if(ciclo != null){
                        if (dbRepo.insertContagem(ciclo, contagem)) {
                            clearReadTags();
                        }
                    }
                    
                } else {
                    clearReadTags();
                }
                
            }

            if(bufferTagsReading.size() > 0){
                clearReadTags();
            }
        }
    }
    
    public static int getCountClassificacao(){
        int count = 0;
        ContagemClassificacao classificacao;
        for(Map.Entry<Integer, ContagemClassificacao> temp : contagem.entrySet()) {
            
            classificacao = temp.getValue();
            count += classificacao.getContador();
            
        }
        
        return count;
    }
    
    private static void startReadings(){
        if(reader != null){ // caso haja um reader para o reader
            reader.stopReader();
        }
        reader = RFIDFactory.getReader(); // instancia um novo reader
        reader.startReader(); // inicia a leitura
    }
    
    private static void stopReadings(){
        if(reader != null){
            reader.stopReader(); // termina a leitura
        }
    }
    
    private static boolean isRunning(){
        if(reader != null){
            return reader.isRunning(); // devolve se está a ler ou não
        }
        return false;
    }

}
