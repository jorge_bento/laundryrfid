/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.mainservice.factory.rfidreader.types;

import com.bitcliq.laundryrfid.mainservice.factory.rfidreader.RFIDReader;
import com.bitcliq.laundryrfid.common.listener.TagListener;
import com.bitcliq.laundryrfid.common.models.Config;
import com.bitcliq.laundryrfid.common.models.Tag;
import com.bitcliq.laundryrfid.common.util.GeneralUtils;
import com.bitcliq.laundryrfid.common.util.Singleton;
import com.module.interaction.ReaderHelper;
import com.rfid.RFIDReaderHelper;
import com.rfid.ReaderConnector;
import com.rfid.config.CMD;
import com.rfid.rxobserver.RXObserver;
import com.rfid.rxobserver.bean.RXInventoryTag;
import java.util.ArrayList;
import java.util.Observer;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author andre
 */
public class RFIDHardware implements RFIDReader {
    
    boolean isRunning = true;
    private static Logger LOG = GeneralUtils.getLogger();
    private static Config config = Singleton.getInstance().getConfig();
    private static RFIDReaderHelper mReaderHelper;
    private static ReaderConnector mConnector;
    private static TagListener listener;
    private static ArrayList<String> tagsList = new ArrayList<>();
    
    @Override
    public void startReader() {
        
        LOG.info("A iniciar o Leitor por Hardware");
        
        isRunning = false;
        
       
            
        mConnector = new ReaderConnector();
        mReaderHelper = (RFIDReaderHelper) mConnector.connectCom(config.getSerialPort(), config.getBoudRate());
        if(mReaderHelper != null){
            
            LOG.info("Connected to RFID with success!!!!!");
            mReaderHelper.resetInventoryBuffer((byte) 0xFF);
            
            
            
            isRunning = true;
            listener = Singleton.getInstance().getTagListener();
            try {
                
                mReaderHelper.registerObserver(mObserver);
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            isRunning = false;
            LOG.fatal("Error while connecting to RFID");
            mConnector.disConnect();
        }
        
    }

    @Override
    public void stopReader() {
        if(isRunning){
            mReaderHelper.unRegisterObserver(mObserver);
            mConnector.disConnect();
            isRunning = false;
        }
        
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }
    
    static Observer mObserver = new RXObserver() {
        @Override
        protected void onInventoryTag(RXInventoryTag found) {
            if(listener != null){
                String formattedTag = found.strEPC.replaceAll("\\s+", "");
                Tag tag = new Tag();
                tag.setTagID(formattedTag);
                listener.foundTag(tag);
                
            }
        }
        
    };
}
