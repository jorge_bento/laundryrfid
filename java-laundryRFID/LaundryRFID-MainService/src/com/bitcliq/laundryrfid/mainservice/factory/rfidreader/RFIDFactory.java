/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.mainservice.factory.rfidreader;

import com.bitcliq.laundryrfid.mainservice.factory.rfidreader.types.RFIDHardware;
import com.bitcliq.laundryrfid.mainservice.factory.rfidreader.types.RFIDMock;
import com.bitcliq.laundryrfid.common.util.Singleton;

/**
 *
 * @author andre
 */
public class RFIDFactory {

    public static RFIDReader getReader(){
        
        // se a leitura selecionada for mockup devolve o Reader por Mockup
        if(Singleton.getInstance().isMockRFIDreader()){
            return new RFIDMock();
        } else {
            return new RFIDHardware();
        }
    }
    
    

}
