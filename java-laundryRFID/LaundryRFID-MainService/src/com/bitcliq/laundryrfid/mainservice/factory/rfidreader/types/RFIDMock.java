/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.mainservice.factory.rfidreader.types;

import com.bitcliq.laundryrfid.mainservice.factory.rfidreader.RFIDReader;
import com.bitcliq.laundryrfid.common.listener.TagListener;
import com.bitcliq.laundryrfid.common.models.MockupReadingTypes;
import com.bitcliq.laundryrfid.common.models.Tag;
import com.bitcliq.laundryrfid.common.util.DatabaseRepo;
import com.bitcliq.laundryrfid.common.util.GeneralUtils;
import com.bitcliq.laundryrfid.common.util.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author andre
 */
public class RFIDMock implements RFIDReader {

    private ArrayList<Tag> tagsComFornecedor = new ArrayList<>(); // lista temporária com tags com fornecedor
    private ArrayList<Tag> tagsComTipoDeRoupa = new ArrayList<>(); // lista temporária com tags registadas com roupa
    private final Singleton singleton = Singleton.getInstance();
    private final DatabaseRepo dbRepo = new DatabaseRepo();
    private boolean keepGoing = false; 
    private final Logger LOG = GeneralUtils.getLogger();

    @Override
    public void startReader() {
        TagListener listener = singleton.getTagListener();
        keepGoing = true;
        int type = singleton.getMockupReadingType();
        if(type == MockupReadingTypes.MK_TAGS_REGISTADAS_FORNECEDOR) {
            tagsComFornecedor = dbRepo.getTagsComFornecedor(50); 
        } else if (type == MockupReadingTypes.MK_TAGS_REGISTADAS_TOTALIDADE) {
            tagsComTipoDeRoupa = dbRepo.getTagsComTipoRoupa(50);
        }

        // lança 10 tags de uma vez no inicio
        for (int i = 0; i < 10; i++) {
            Tag tag = randomTag();
            if (keepGoing && tag != null) {
                listener.foundTag(tag);
            }
        }

        // calcula um numero entre 10 e 20 (tags a ser enviadas na "second wave"
        // estas tags vão ser enviadas entre 1 segundo e 3 segundos após as 10 tags iniciais
        int secondWave = GeneralUtils.randomInt(10, 20);
        for (int i = 0; i < secondWave; i++) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Tag tag = randomTag();
                    if (keepGoing && tag != null) {
                        listener.foundTag(tag);
                    }
                    this.cancel();
                }
            }, GeneralUtils.randomInt(1000, 3000));
        }

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                this.cancel();
                if (keepGoing) {
                    startReader(); // reinicia o ciclo de leitura
                }
            }
        }, 20000);

    }

    @Override
    public void stopReader() {
        keepGoing = false;
    }

    @Override
    public boolean isRunning() {
        return keepGoing;
    }

    private Tag randomTag() {
        Tag tag = null;
        int type = singleton.getMockupReadingType();
        if (type == MockupReadingTypes.MK_TAGS_NAO_REGISTADAS) { // caso esteja selecionada as tags não registadas na BD
            LOG.debug("A gerar uma tag que não está registada na base de dados");
            String generated = "";
            do { // vai gerando uma tag aleatória até ela não existir na BD
                generated = generateRandomString();
            } while (dbRepo.isTagRegistered(generated));
            tag = new Tag();
            tag.setTagID(generated);
            
        } else if (type == MockupReadingTypes.MK_TAGS_REGISTADAS_FORNECEDOR) { // caso esteja selecionada as tags registadas só com dados do fornecedor
            LOG.debug("A gerar uma tag que está registada na base de dados");
            if(tagsComFornecedor.size() <= 0){ // verifica se existem tags guardadas localmente
                tagsComFornecedor = dbRepo.getTagsComFornecedor(50); // se não tiver vai buscar
            }
            
            if(tagsComFornecedor.size() > 0)
                tag = tagsComFornecedor.get(GeneralUtils.randomInt(0, tagsComFornecedor.size())); // obtem uma aleatória
            
        } else if (type == MockupReadingTypes.MK_TAGS_REGISTADAS_TOTALIDADE) { // tags registadas com um tipo de roupa e dados do fornecedor
            LOG.debug("A gerar uma tag que está registada na base de dados na totalidade");
            if(tagsComTipoDeRoupa.size() <= 0){
                tagsComTipoDeRoupa = dbRepo.getTagsComTipoRoupa(50);
            }
            
            if(tagsComTipoDeRoupa.size() > 0)
                tag = tagsComTipoDeRoupa.get(GeneralUtils.randomInt(0, tagsComTipoDeRoupa.size()));
        }
        
        return tag;
        
    }

    // gera uma tag aleatória
    private String generateRandomString() {
        String possible = "0123456789ABCDEF", ret = "E2";

        for (int i = 0; i < 22; i++) { // gera uma tag com 22 caracteres aleatórios
            ret += possible.charAt(GeneralUtils.randomInt(0, possible.length() - 1));
        }

        return ret;
    }
}
