/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitcliq.laundryrfid.mainservice.factory.rfidreader;

/**
 *
 * @author andre
 */
public interface RFIDReader {
    void startReader();
    void stopReader();
    boolean isRunning();
}
